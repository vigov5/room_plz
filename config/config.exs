# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :room_plz,
  ecto_repos: [RoomPlz.Repo]

# Configures the endpoint
config :room_plz, RoomPlz.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aGhtaBwXvX8nOp31kCS0pDVIvY/yqJTUrP1aqjeup7LgcKaTqdDEMEekKthoCt5t",
  render_errors: [view: RoomPlz.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RoomPlz.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Do not include metadata nor timestamps in development logs
config :logger,
       backends: [:console, Logger.Backends.ErrorMail]

# Do not print debug messages
config :logger, :console,
       format: "$time $metadata[$level] $message\n",
       metadata: [:id]

config :logger, :error_mail,
       format: "$date $time $metadata[$level] $message\n",
       from: {"Framgia Local Server", "error-log@scheduling.framgia.vn"},
       to: [{"Nguyen Dinh Tung", "nguyen.dinh.tung@framgia.com"}, {"Nguyen Anh Tien", "nguyen.anh.tien@framgia.com"}],
       metadata: [:id]
config :room_plz, :sender, {"Framgia Scheduling", "no-reply@framgia.com"}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: RoomPlz.User,
  repo: RoomPlz.Repo,
  module: RoomPlz,
  logged_out_url: "/",
  email_from_name: "Framgia Booking Schedule",
  email_from_email: "no-reply@framgia.com",
  opts: [:authenticatable, :recoverable, :lockable, :trackable, :unlockable_with_token, :confirmable, :registerable, :rememberable],
  user_token: true

# %% End Coherence Configuration %%

config :canary,
  repo: RoomPlz.Repo,
  unauthorized_handler: {RoomPlz.ControllerHelpers, :handle_unauthorized},
  not_found_handler: {RoomPlz.ControllerHelpers, :handle_not_found}

config :room_plz, :append_to_event_id, "googlecalendar"
config :room_plz, :timezone_location, "Asia/Ho_Chi_Minh"
config :room_plz, :google_api_scope, [scope: "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/userinfo.email"]
config :room_plz, :open_id_api, "https://www.googleapis.com/plus/v1/people/me/openIdConnect"

config :google_calendar, :base_url, "https://www.googleapis.com/calendar/v3"
config :google_calendar, :content_type, [{"content-type", "application/json"}]