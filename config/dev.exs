use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :room_plz, RoomPlz.Endpoint,
  http: [port: 8000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]


# Watch static and templates for browser reloading.
config :room_plz, RoomPlz.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :room_plz, RoomPlz.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "room_plz_dev",
  hostname: "localhost",
  pool_size: 10

config :coherence, RoomPlz.Coherence.Mailer,
       adapter: Swoosh.Adapters.Local

import_config "dev.secret.exs"

#  Example of dev.scret.exs

#  config :google_calendar, Google,
#    client_id: GOOGLE_CLIENT_ID,
#    client_secret: GOOGLE_CLIENT_SECRET,
#    redirect_uri: "https://your_site.*/auth/google/callback"

#  config :room_plz, WSM,
#    client_id: WSM_CLIENT_ID,
#    client_secret: WSM_CLIENT_SECRET,
#    redirect_uri: "http://your_site.*/auth/wsm/callback"