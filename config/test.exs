use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :room_plz, RoomPlz.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :coherence, RoomPlz.Coherence.Mailer,
       adapter: Swoosh.Adapters.Local

# Configure your database
config :room_plz, RoomPlz.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "room_plz_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
