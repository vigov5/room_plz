defmodule RoomPlz.RoomController do
  use RoomPlz.Web, :controller

  alias RoomPlz.{Room, Branch}

  plug :scrub_params, "room" when action in [:create, :update]
  plug :load_and_authorize_resource, model: Room, except: [:suggestion]
  plug :load_resource, model: Branch, except: [:index, :get_all_branches, :suggestion], id_name: "branch_id", persisted: true
  plug :get_all_branches, "room" when action in [:new, :create, :edit, :update]
  plug :get_client when action in [:update]

  def index(conn, %{"branch_id" => branch_id}) do
    branch = Repo.get!(Branch, branch_id)
    rooms =
      Room
      |> Room.from_branch(branch)
      |> Repo.all()
      |> Repo.preload(:branch)

    render(conn, "index.html", rooms: rooms, branch: branch)
  end

  def new(conn, _params) do
    changeset = Room.changeset(%Room{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"room" => room_params}) do
    changeset = Room.changeset(%Room{}, room_params)
    case Repo.insert(changeset) do
      {:ok, _room} ->
        conn
        |> put_flash(:info, "Room created successfully.")
        |> redirect(to: branch_room_path(conn, :index, conn.assigns.branch))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    render(conn, "show.html")
  end

  def edit(conn, _params) do
    changeset = Room.changeset(conn.assigns.room)
    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"room" => room_params}) do
    client = conn.assigns.client
    room_params = Map.merge(room_params, %{"client" => client})
    changeset = Room.changeset(conn.assigns.room, room_params)

    case Repo.update(changeset) do
      {:ok, room} ->
        conn
        |> put_flash(:info, "Room updated successfully.")
        |> redirect(to: branch_room_path(conn, :show, conn.assigns.branch, room))
      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(conn.assigns.room)

    conn
    |> put_flash(:info, "Room deleted successfully.")
    |> redirect(to: branch_room_path(conn, :index, conn.assigns.branch))
  end

  defp get_all_branches(conn, _options) do
    branches = Branch.all_for_select()
    assign(conn, :branches, branches)
  end

  def suggestion(conn, _params) do
    branches = Branch.all_for_select()

    conn
    |> render("suggestion.html", branches: branches)
  end
end
