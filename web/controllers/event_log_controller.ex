defmodule RoomPlz.EventLogController do
  use RoomPlz.Web, :controller

  alias RoomPlz.{EventLog, Branch}

  def index(conn, %{"branch_id" => branch_id}) do
    branch = Repo.get!(Branch, branch_id)

    event_logs =
      EventLog
      |> EventLog.from_branch(branch)
      |> Repo.all()
      |> Repo.preload([:event, :user])

    render(conn, "index.html", event_logs: event_logs, branch: branch)
  end

  def create(params) do
    changeset = EventLog.changeset(%EventLog{}, params)
    Repo.insert!(changeset)
  end
end
