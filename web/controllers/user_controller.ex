defmodule RoomPlz.UserController do
  use RoomPlz.Web, :controller
  alias RoomPlz.{User, Event, ModelHelpers, Team}
  alias Coherence.Config

  plug :load_and_authorize_resource, model: User, except: [:user_requests]
  plug :is_same_user, %{} when action in [:user_requests]
  plug :get_teams when action in [:new, :create, :edit, :update]

  def index(conn, _params) do
    users =
      User
      |> Repo.all()
      |> Repo.preload(:branch)
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        # confirm users if users are created by admin
        if String.contains?(conn.request_path, "users") do
          Coherence.ControllerHelpers.confirm!(user)
        end

        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: user_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user =
      User
      |> Repo.get!(id)
      |> Repo.preload(:branch)

    invitation_list = if user.invitation_list, do: Regex.replace(~r/,/, user.invitation_list, ", "), else: ""
    user = Map.merge(user, %{invitation_list: invitation_list})
    team = if user.team_id, do: Repo.get(Team, user.team_id), else: %{name: ""}

    render(conn, "show.html", user: user, team: team)
  end

  def edit(conn, %{"id" => id}) do
    user = Repo.get!(User, id)
    current_user = conn.assigns.current_user
    changeset = User.changeset(user)
    is_owner = user.id == current_user.id
    render(conn, "edit.html", user: user, changeset: changeset, is_owner: is_owner)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    current_user = conn.assigns.current_user
    user = Repo.get!(User, id)
    is_owner = user.id == current_user.id

    changeset =
      # prevent normal users from changing role or email fields
      if is_authorized(current_user, user_params) do
        User.changeset(user, user_params)
      else
        {:unauthorized}
      end

    if changeset == {:unauthorized} do
      redirect_with_error(conn)
    else
      case Repo.update(changeset) do
        {:ok, user} ->
          if user.id == conn.assigns.current_user.id do
            apply(Config.auth_module, Config.update_login, [conn, user, [id_key: Config.schema_key]])
          end

          conn
          |> put_flash(:info, "User updated successfully.")
          |> redirect(to: user_path(conn, :show, user))
        {:error, changeset} ->
          render(conn, "edit.html", user: user, changeset: changeset, is_owner: is_owner)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: user_path(conn, :index))
  end

  def user_requests(conn, %{"user_id" => user_id} = params) do
    events =
      Event
      |> ModelHelpers.sort_by_id()
      |> ModelHelpers.with_limit(params)
      |> Repo.all()
      |> Repo.preload([:creator, room: :branch])
      |> Enum.filter(&is_user(&1, conn.assigns.current_user))

    render(conn, "user_requests.html", events: events, user_id: user_id)
  end

  def is_same_user(conn, _opts) do
    user = Repo.get!(User, conn.params["user_id"])
    current_user = conn.assigns.current_user
    if current_user.id == user.id do
      conn
    else
      redirect_with_error(conn)
    end
  end

  defp is_authorized(current_user, user_params) do
    User.is_super_admin?(current_user) ||
    (current_user.role == user_params["role"] && current_user.email == user_params["email"])
  end

  defp is_user(event, user) do
    collaborators = if event.collaborators, do: String.split(event.collaborators, ","), else: []
    user.id == event.creator.id || Enum.any?(collaborators, fn x -> x == user.email end)
  end
end
