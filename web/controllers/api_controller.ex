defmodule RoomPlz.APIController do
  use RoomPlz.Web, :controller
  use Timex
  alias GoogleCalendar.Event, as: GEvent
  alias RoomPlz.{Event, Branch, Room, Slot, Car, User,
                 ModelHelpers, EventController}

  plug :load_and_authorize_resource, model: Event, only: [:update_event]
  plug :load_resource, model: Branch, only: [:import_events], id_name: "branch_id"
  plug :get_client, %{} when action in [:synchronize, :import_events]

  @append_to_event_id Application.get_env(:room_plz, :append_to_event_id)
  @time_min [days: -1] # Import all single events from today
  @max_results 2500 # Maximum imports that google allows are 2500
  @frequency %{"Daily" => "day(s)", "Weekly" => "week(s)", "Monthly" => "month(s)", "Yearly" => "year(s)"}

  def show_branch(conn, %{"id" => id}) do
    branch =
      Branch
      |> Repo.get!(id)
      |> Repo.preload(:rooms)

    render(conn, "branch.json", branch: branch)
  end

  def rooms(conn, %{"branch_id" => id}) do
    branch = Repo.get!(Branch, id)
    rooms =
      Room
        |> Room.from_branch(branch)
        |> Room.asc_by_name
        |> Repo.all

    render(conn, "rooms.json", rooms: rooms)
  end

  def users(conn, _params) do
    users = Repo.all(User)

    render(conn, "email_list.json", email_list: users)
  end

  def get_invitation_list(conn, %{"id" => id}) do
    user = Repo.get(User, id)
    invitation_list =
      if user.invitation_list do
        user.invitation_list
        |> String.split(",")
        |> Enum.map(fn email -> %{email: email} end)
      else
        []
      end

    render(conn, "email_list.json", email_list: invitation_list)
  end

  def cars(conn, _params) do
    cars =
      Car
      |> Car.asc_by_name
      |> Repo.all

    render(conn, "cars.json", cars: cars)
  end

  def slots(conn, %{"start" => start_date, "end" => end_date}) do
    start_date = date_str_to_ecto(start_date, :utc)
    end_date = date_str_to_ecto(end_date, :utc)

    slots =
      Slot
      |> ModelHelpers.is_status("approved")
      |> ModelHelpers.in_range(start_date, end_date)
      |> Repo.all
      |> Repo.preload([:creator, :car])

    render(conn, "slots.json", slots: slots)
  end

  def get_slot(conn, %{"id" => id}) do
    slot =
      Slot
      |> Repo.get(id)
      |> Repo.preload([:creator, :car])

    render(conn, "slot.json", slot: slot)
  end

  # Import events from current google calendar
  def import_events(conn, %{"source_calendar_id" => source_calendar_id}) do
    client = conn.assigns.client

    time_min =
      DateTime.utc_now()
      |> Timex.shift(@time_min)
      |> DateTime.to_naive()
      |> naive_to_local_datetime(:time_str)
    opts = [params: [maxResults: @max_results, timeMin: time_min, showDeleted: false]]

    # Get event list on imported calendar
    {:ok, _action, %{"items" => event_list}} = GEvent.list(client, %{calendar_id: source_calendar_id}, opts)

    event_list
    |> Enum.map(&insert_event_to_local(&1, conn))
    |> Enum.filter(&(&1))
    |> Enum.each(&insert_event_to_google(&1, conn))

    show_resp("Event import process has finished", conn, conn.assigns.branch.id)
  end

  defp insert_event_to_local(%{"recurringEventId" => _id}, _conn) do
    nil
  end

  defp insert_event_to_local(%{"status" => "confirmed", "start" => %{"dateTime" => _start}} = event, conn) do
    user = conn.assigns.current_user
    branch_id = conn.assigns.branch.id
    case create_event(event, user, branch_id) do
      %{"is_past?" => true} -> nil
      event_params ->
        changeset = Event.changeset(%Event{}, event_params)
        case Repo.insert(changeset) do
          {:ok, event} ->
            # create log
            event
            |> Repo.preload([room: :branch])
            |> EventController.initiate_log(conn, event_params["action"])

            event
          {:error, _changeset} ->
            nil
        end
    end
  end

  defp insert_event_to_local(_event, _conn) do
    nil
  end

  defp insert_event_to_google(event, conn) do
    g_event = initiate_event(event)

    GEvent.insert(conn.assigns.client, g_event)
  end

  defp create_event(event, user, branch_id) do
    # add recurrence rule if available
    event =
      if event["recurrence"] do
        recurrence_rule =
          event["recurrence"]
          |> Enum.filter(&String.contains?(&1, "RRULE"))
          |> List.first()

        recurrence =
          recurrence_rule
          |> String.split(";")
          |> Enum.map(&String.split(&1, "="))
          |> Map.new(fn [k, v] -> {k, v} end)

        parsed_recurrence = get_params(recurrence)
        repeat = get_repeat_rule(parsed_recurrence)

        # Compare last day of recurrence with today
        if is_current_event?(event, repeat) do
          Map.merge(event, repeat)
        else
          Map.merge(event, %{"is_past?" => true})
        end
      else
        event
      end

    # add room id
    summary = event["summary"] || ""
    default_id =
      Room
      |> Repo.get_by(%{branch_id: branch_id, name: "Other"})
      |> Map.get(:id)

    # Get Room Name from Title, separated by ":", "_" or "-"
    {room_id, title} =
      cond do
        Regex.run(~r/\[.*\]/, summary) -> get_details(summary, default_id, branch_id, Regex.run(~r/\[(.*)\](.*)/, summary))
        String.contains?(summary, ":") -> get_details(summary, default_id, branch_id, Regex.run(~r/(.*):(.*)/, summary))
        String.contains?(summary, "_") -> get_details(summary, default_id, branch_id, Regex.run(~r/(.*)_(.*)/, summary))
        String.contains?(summary, "-") -> get_details(summary, default_id, branch_id, Regex.run(~r/(.*)-(.*)/, summary))
        true -> {default_id, event["summary"]}
      end

    # add other parameters
    Map.merge(event, %{"creator_id" => user.id, "room_id" => room_id, "action" => "approved",
                        "status" => "approved", "title" => title, "description" => "",
                        "start" => local_date_str_to_datetime(event["start"]["dateTime"]),
                        "end" => local_date_str_to_datetime(event["end"]["dateTime"]),
                        "is_imported" => true})
  end

  defp get_params(recurrence) do
    frequency = String.capitalize(recurrence["RRULE:FREQ"])
    interval = if recurrence["INTERVAL"], do: String.to_integer(recurrence["INTERVAL"]), else: 1

    duration =
      cond do
        recurrence["COUNT"] ->
          String.to_integer(recurrence["COUNT"])
        recurrence["UNTIL"] ->
          # Convert string from 20170609T120000Z to 2017-06-09T12:00:00Z
          regex = ~r/([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})Z/

          regex
          |> Regex.replace(recurrence["UNTIL"], "\\1-\\2-\\3T\\4:\\5:\\6Z")
          |> Ecto.DateTime.cast!()
          |> ecto_to_local_datetime(:date_str)
          |> Ecto.Date.cast!()
        true ->
          nil
      end

    [frequency, interval, duration, recurrence["BYDAY"]]
  end

  defp get_repeat_rule([frequency, interval, duration, weekdays] = parsed_recurrence) do
    readable_rule = get_readable_rule(parsed_recurrence)
    repeat = %{"repeat" => readable_rule,
              "frequency" => frequency,
              "interval" => interval}
    repeat =
      cond do
        is_integer(duration) ->
          Map.merge(repeat, %{"count" => duration})
        not is_nil(duration) ->
          Map.merge(repeat, %{"until" => duration})
        true ->
          repeat
      end

    if weekdays, do: Map.merge(repeat, %{"weekday" => weekdays}), else: repeat
  end

  defp get_readable_rule([frequency, interval, duration, weekdays]) do
    rule = "Every #{interval} #{@frequency[frequency]}"

    rule =
      cond do
        is_integer(duration) ->
          "#{rule} #{duration} times"
        is_binary(duration) ->
          "#{rule} until #{duration}"
        true ->
          rule
      end

    if weekdays, do: "#{rule} on #{weekdays}", else: rule
  end

  defp is_current_event?(event, repeat) do
    start_date =
      ~r/(.*)T(.*)/
      |> Regex.replace(event["start"]["dateTime"], "\\1")
      |> Date.from_iso8601!()

    date_array =
      repeat
      |> Map.new(fn {k, v} -> {String.to_atom(k), v} end)
      |> Event.check_and_create_date_array(%{date: start_date})

    length = Enum.count(date_array)

    if length > 0 do
      last_day =
        date_array
        |> Enum.at(length - 1)
        |> Date.from_iso8601!()

      today = ecto_to_local_datetime(Ecto.DateTime.utc(), :date)

      Date.compare(last_day, today) != :lt
    else
      false
    end
  end

  defp get_details(summary, default_id, branch_id, [_, room_name, title]) do
    case Repo.get_by(Room, %{name: normalize(room_name), branch_id: branch_id}) do
      %{id: id} ->
        {id, normalize(title)}
      nil ->
        {default_id, summary}
    end
  end

  defp get_details(summary, default_id, _, nil) do
    {default_id, summary}
  end

  defp normalize(name) do
    name
    |> String.capitalize()
    |> String.trim()
  end

  # Synchronization
  def synchronize(conn, %{"branch_id" => branch_id, "date_time" => start_time_sync} = params) do
    client = conn.assigns.client
    branch = Repo.get!(Branch, branch_id)
    calendar_id = branch.calendar_id
    opts = [params: [maxResults: @max_results, timeMin: start_time_sync, showDeleted: true]]

    # Get local and google events
    local_events = get_events(params)

    google_events =
      case GEvent.list(client, %{"calendar_id": calendar_id}, opts) do
        {:ok, _action, resp} ->
          Enum.filter(resp["items"], fn event -> String.contains?(event["id"], "googlecalendar") end)
        {:error, code, resp} ->
          conn
          |> render(RoomPlz.ChangesetView, "event_error.json", error: "#{code} - #{resp}")
      end

    # Check if events exists in local but not on google => to insert
    inserted_count =
      local_events
      |> Enum.map(&check_exists_in_google(&1, google_events, conn))
      |> Enum.count(&(!&1))

    # Check if events exists in google but was changed in local => to update/ delete
    updated_count =
      google_events
      |> Enum.map(&check_different_and_update(&1, local_events, conn))
      |> Enum.count(&(&1))

    message =
      if inserted_count > 0 || updated_count > 0 do
        if inserted_count > 0, do: "#{inserted_count} event(s) inserted. ", else: "" <>
        if updated_count > 0, do: "#{updated_count} event(s) updated", else: ""
      else
        "Local and google events are the same. Don't need to synchronize"
      end

    render(conn, RoomPlz.ChangesetView, "event_success.json", success: message)
  end

  defp get_events(%{"branch_id" => id, "date_time" => date_time}) do
    branch = Repo.get!(Branch, id)

    events =
      Event
      |> Event.with_room_and_branch
      |> Event.from_branch(branch)
      |> ModelHelpers.from_date(date_time)
      |> ModelHelpers.is_status("approved")
      |> Repo.all

    Enum.map(events, &update_event_id(&1))
  end

  defp update_event_id(event) do
    id = @append_to_event_id <> Integer.to_string(event.id)
    Map.merge(event, %{id: id})
  end

  defp check_exists_in_google(local_event, google_events, conn) do
    is_existed? =
      google_events
      |> Enum.map(fn(google_event) -> google_event["id"] end)
      |> Enum.member?(local_event.id)

    if is_existed? do
      true
    else
      event = initiate_event(local_event)

      conn.assigns.client
      |> GEvent.insert(event)
      |> render_error_if_exist(conn, event)

      false
    end
  end

  defp check_different_and_update(google_event, local_events, conn) do
    local_events
    |> Enum.map(&is_different?(&1, google_event))
    |> Enum.map(&update_google_event(&1, conn))
    |> Enum.all?(fn(x) -> x == %{updated: true} end)
  end

  defp is_different?(%{id: id, status: "approved"} = local_event, %{"id" => id} = google_event) do
    local_recurrence = if local_event.repeat, do: [get_recurrence_rule(local_event)], else: nil
    local_start = ecto_to_local_datetime(local_event.start, :full_str)
    local_end = ecto_to_local_datetime(local_event.end, :full_str)

    google_recurrence =
      if google_event["recurrence"] do
        google_event["recurrence"]
        |> Enum.filter(&String.contains?(&1, "RRULE"))
      else
        nil
      end

    is_different =
      local_event.title != google_event["summary"] ||
      local_start != google_event["start"]["dateTime"] ||
      local_end != google_event["end"]["dateTime"] ||
      local_recurrence != google_recurrence

    {is_different, local_event}
  end

  defp is_different?(%{id: id, status: "rejected"} = local_event, %{"id" => id, "status" => "confirmed"} = _google_event) do
    {true, local_event}
  end

  defp is_different?(_, _) do
    {false, %{}}
  end

  defp update_google_event({true, %{status: "approved"} = local_event}, conn) do
    event = initiate_event(local_event)

    conn.assigns.client
    |> GEvent.update(event)
    |> render_error_if_exist(conn, event)

    %{updated: true}
  end

  defp update_google_event({true, %{status: "rejected"} = local_event}, conn) do
    event = initiate_event(local_event)

    conn.assigns.client
    |> GEvent.delete(event)
    |> render_error_if_exist(conn, event)

    %{updated: true}
  end

  defp update_google_event({false, _}, _) do
    %{updated: false}
  end

  # Room Suggestions
  def suggestion(conn, %{"start" => start_time, "end" => end_time, "time_length" => "day"} = params) do
    time = {start_time, end_time}
    {_, rooms} = get_room_available(time, params)

    render(conn, "rooms.json", rooms: rooms)
  end

  def suggestion(conn, %{"start" => start_time, "end" => end_time, "time_length" => time_length} = params) do
    start_date = date_str_to_datetime(start_time, :date)
    start_time_str = date_str_to_datetime(start_time, :time_str)
    end_time_str = date_str_to_datetime(end_time, :time_str)

    until = if time_length == "week", do: [weeks: 1], else: [months: 1]
    interval = Interval.new(from: start_date, until: until, right_open: false)

    dates_with_rooms =
      interval
      |> Interval.with_step([days: 1])
      |> Enum.map(&Timex.format!(&1, "%Y-%m-%d", :strftime))
      |> Enum.map(&get_room_available(&1, {start_time_str, end_time_str}, params))

    render(conn, "dates_with_rooms.json", dates: dates_with_rooms)
  end

  def suggestion(conn, %{"duration" => duration} = params) do
    start_time_list = generate_time(:start, duration)
    end_time_list = generate_time(:end, duration)
    time_list = Enum.zip(start_time_list, end_time_list)

    time_with_rooms_list = Enum.map(time_list, &get_room_available(&1, params))

    available_rooms =
      time_with_rooms_list
      |> Enum.map(fn {time, rooms} -> Enum.map(rooms, fn(room) -> [time, room] end) end)
      |> Enum.flat_map(&(&1))
      |> Enum.group_by(&List.last/1, &List.first/1)

    render(conn, "rooms.json", rooms: available_rooms)
  end

  defp get_room_available(date_str, {start_str, end_str}, params) do
    start_time = "#{date_str}T#{start_str}Z"
    end_time = "#{date_str}T#{end_str}Z"

    params = Map.merge(params, %{"start" => start_time, "end" => end_time})
    {_, rooms} = get_room_available({start_time, end_time}, params)

    {date_str, rooms}
  end

  defp get_room_available({start_time, end_time} = time, params) do
    params = Map.merge(params, %{"start" => start_time, "end" => end_time})
    branch =
      Branch
      |> Repo.get!(params["branch_id"])
      |> Repo.preload(:rooms)

    rooms =
      branch.rooms
      |> Enum.filter(&validate_room(&1, params))
      |> Enum.sort()

    {time, rooms}
  end

  defp validate_room(room, params) do
    params = Map.merge(params, %{"room_id" => room.id})
    changeset = Event.changeset(%Event{}, params)
    capacity = if params["capacity"], do: String.to_integer(params["capacity"]), else: 0

    changeset.valid? && room.name != "Other" && room.capacity >= capacity
  end

  defp generate_time(:start, duration) do
    [period, start_time, end_time] = get_time_info(duration)
    end_time = Timex.shift(end_time, minutes: -period)

    generate_list(start_time, end_time)
  end

  defp generate_time(:end, duration) do
    [period, start_time, end_time] = get_time_info(duration)
    start_time = Timex.shift(start_time, minutes: period)

    generate_list(start_time, end_time)
  end

  defp get_time_info([hours, minutes, date]) do
    period = convert_to_minutes(hours, minutes)

    # start time from 07:00 to 24:00 each day. -07:00 for UTC timezone
    {:ok, start_time, _} = DateTime.from_iso8601("#{date}T00:00:00Z")
    {:ok, end_time, _} = DateTime.from_iso8601("#{date}T17:00:00Z")

    [period, start_time, end_time]
  end

  defp convert_to_minutes(hours, minutes) do
    String.to_integer(hours) * 60 + String.to_integer(minutes)
  end

  defp generate_list(start_time, end_time) do
    interval = Interval.new(from: start_time, until: end_time, right_open: false)

    interval
    |> Interval.with_step([minutes: 30])
    |> Enum.map(&Timex.format!(&1, "%Y-%m-%dT%H:%M:%SZ", :strftime))
  end
end
