defmodule RoomPlz.AuthController do
  use RoomPlz.Web, :controller
  alias Coherence.Config
  alias RoomPlz.{Branch, User, EmailController}

  @password_length 10

  def gg_authenticate(%{assigns: %{current_user: %{role: "super-admin"}}} = conn, _params) do
    branch_id = get_branch(conn)
    conn = put_session(conn, :branch_id, branch_id)
    scope = Application.get_env(:room_plz, :google_api_scope)

    config = Keyword.merge(scope, [approval_prompt: "force"])

    redirect conn, external: Google.authorize_url!(config)
  end

  def gg_authenticate(conn, _params) do
    redirect_with_error(conn)
  end

  def wsm_authenticate(conn, _params) do
    redirect conn, external: WSM.authorize_url!()
  end

  def gg_callback(conn, %{"code" => code}) do
    client = Google.get_token!(code: code)
    branch_id = get_session(conn, :branch_id)
    open_id = Application.get_env(:room_plz, :open_id_api)

    %{body: %{"email" => auth_email}} = OAuth2.Client.get!(client, open_id)
    refresh_token = client.token.refresh_token

    # update/ insert token
    Branch
    |> Repo.get(branch_id)
    |> Branch.changeset(%{refresh_token: refresh_token, auth_email: auth_email})
    |> Repo.update!()

    conn
    |> put_session(:token, client.token)
    |> redirect(to: branch_calendar_path(conn, :index, branch_id))
  end

  def gg_callback(conn, %{"error" => error}) do
    branch_id = get_session(conn, :branch_id)

    conn
    |> put_flash(:error, error)
    |> redirect(to: branch_calendar_path(conn, :index, branch_id))
  end

  def wsm_callback(conn, %{"code" => code}) do
    case WSM.get_token(code: code) do
      {:ok, client} ->
        body = %{"access_token": client.token.access_token}
        headers = [{"Content-Type", "application/json"}]
        %{body: %{"email" => email, "name" => name}} = OAuth2.Client.post!(client, "https://wsm.framgia.vn/me", body, headers)

        existing_user = Repo.get_by(User, %{email: email})
        {conn, user} =
          if existing_user do
            {conn, existing_user}
          else
            conn = assign(conn, :register, true)
            {:ok, new_user} = insert_user(email, name)

            {conn, new_user}
          end

        log_in_user(conn, user)

        {:error, _error} ->
          conn
          |> put_flash(:error, "Unable to login with WSM at the moment. Please try again later")
          |> redirect(to: session_path(conn, :new))
    end
  end

  defp insert_user(email, name) do
    password =
      @password_length
      |> :crypto.strong_rand_bytes()
      |> Base.url_encode64
      |> binary_part(0, @password_length)

    user = %{"name" => name, "email" => email, "password" => password, "password_confirmation" => password}
    EmailController.send_password(user)

    %User{}
    |> User.changeset(user)
    |> Repo.insert!
    |> Coherence.ControllerHelpers.confirm!
  end

  defp log_in_user(conn, user) do
    Config.auth_module
    |> apply(Config.create_login, [conn, user, [id_key: Config.schema_key]])
    |> show_flash_message()
    |> redirect(to: page_path(conn, :index))
  end

  defp show_flash_message(%{assigns: %{register: true}} = conn) do
    put_flash(conn, :info, "Signed in successfully. Account information has been sent to your email")
  end

  defp show_flash_message(conn) do
    put_flash(conn, :info, "Signed in successfully.")
  end

  def delete(conn, _params) do
    branch_id = get_branch(conn)

    conn
    |> put_flash(:info, "You have been signed out!")
    |> delete_session(:token)
    |> redirect(to: branch_calendar_path(conn, :index, branch_id))
  end

  defp get_branch(conn) do
    url = conn.req_headers
    |> Enum.into(%{})
    |> Map.fetch!("referer")

    ~r/branches\/(.*)/
    |> Regex.run(url)
    |> List.last()
    |> Integer.parse()
    |> elem(0)
  end
end