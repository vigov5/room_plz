defmodule RoomPlz.BranchController do
  use RoomPlz.Web, :controller

  alias RoomPlz.{Branch, Room}
  plug :scrub_params, "branch" when action in [:create, :update]
  plug :load_and_authorize_resource, model: Branch

  @default_room %{"name" => "Other", "color" => "orange", "capacity" => 0, "overlap_acceptance" => true}

  def index(conn, _params) do
    branches = Repo.all(Branch)
    render(conn, "index.html", branches: branches)
  end

  def new(conn, _params) do
    changeset = Branch.changeset(%Branch{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"branch" => branch_params}) do
    changeset = Branch.changeset(%Branch{}, branch_params)

    case Repo.insert(changeset) do
      {:ok, branch} ->
        room = Map.merge(@default_room, %{"branch_id" => branch.id})

        %Room{}
        |> Room.changeset(room)
        |> Repo.insert()

        conn
        |> put_flash(:info, "Branch created successfully.")
        |> redirect(to: branch_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    branch = Repo.get!(Branch, id)
    render(conn, "show.html", branch: branch)
  end

  def edit(conn, _params) do
    changeset = Branch.changeset(conn.assigns.branch)
    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"branch" => branch_params}) do
    changeset = Branch.changeset(conn.assigns.branch, branch_params)

    case Repo.update(changeset) do
      {:ok, branch} ->
        conn
        |> put_flash(:info, "Branch updated successfully.")
        |> redirect(to: branch_path(conn, :show, branch))
      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(conn.assigns.branch)

    conn
    |> put_flash(:info, "Branch deleted successfully.")
    |> redirect(to: branch_path(conn, :index))
  end
end
