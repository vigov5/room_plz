defmodule RoomPlz.ControllerHelpers do
  import Phoenix.Controller
  import RoomPlz.{Router.Helpers, Tools}
  import Plug.Conn
  alias RoomPlz.{Repo, Branch, ModelHelpers, Team, User}
  alias Ecto.DateTime

  @append_to_event_id Application.get_env(:room_plz, :append_to_event_id)

  def handle_unauthorized(conn) do
    conn
    |> put_flash(:error, "You don't have enough permission")
    |> redirect(to: session_path(conn, :new))
    |> halt()
  end

  def redirect_with_error(%{params: %{"event" => %{"form_type" => _type}}} = conn) do
    conn
    |> render(RoomPlz.ChangesetView, "event_error.json", error: "You don't have enough permission")
  end

  def redirect_with_error(conn) do
    conn
    |> put_flash(:error, "You don't have enough permission")
    |> redirect(to: page_path(conn, :index))
    |> halt()
  end

  def handle_not_found(conn) do
    conn
    |> put_status(404)
    |> render(RoomPlz.ErrorView, "404.html")
    |> halt()
  end

  def scrub_get_params(conn, _opts) do
     params = conn.params |> Enum.reduce(%{}, &scrub/2)
     %{conn | params: params}
  end

  defp scrub({k, ""}, acc) do
    Map.put(acc, k, nil)
  end

  defp scrub({k, v}, acc) do
    Map.put(acc, k, v)
  end

  def get_client(conn, _opts) do
    branch_id = conn.params["branch_id"]
    branch = Repo.get(Branch, branch_id)
    token = get_session(conn, :token) || OAuth2.AccessToken.new("")

    if branch.refresh_token do
      client =
        if OAuth2.AccessToken.expired?(token) || token.expires_at == nil do
          # get token
          token = Map.merge(token, %{refresh_token: branch.refresh_token})

          Google.client()
          |> Map.merge(%{token: token})
          |> OAuth2.Client.refresh_token!()
        else
          Map.merge(Google.client(), %{token: token})
        end

      conn
      |> put_session(:token, client.token)
      |> assign(:client, client)
    else
      error = "Admin needs to authorize through google account first"
      if conn.params["event"]["form_type"] do
        conn
        |> render(RoomPlz.ChangesetView, "event_error.json", error: error)
        |> halt()
      else
        conn
        |> put_flash(:error, error)
        |> redirect(to: branch_calendar_path(conn, :index, branch_id))
        |> halt()
      end
    end
  end

  def show_resp(message, %{params: %{"event" => %{"form_type" => _type}}} = conn, _branch_id) when is_binary(message) do
    conn
    |> render(RoomPlz.ChangesetView, "event_success.json", success: message)
  end

  def show_resp(message, conn, branch_id) when is_binary(message) do
    conn
    |> put_flash(:info, message)
    |> redirect(to: branch_calendar_path(conn, :index, branch_id))
  end

  def show_resp(function, %{params: %{"event" => %{"form_type" => _type}}} = conn, _branch_id) do
    case function do
      {:ok, action, _resp} ->
        conn
        |> render(RoomPlz.ChangesetView, "event_success.json", success: action)
      {:error, code, resp} ->
        conn
        |> render(RoomPlz.ChangesetView, "event_error.json", error: "#{code} - #{resp}")
    end
  end

  def show_resp(function, conn, branch_id) do
    case function do
      {:ok, action, _resp} ->
        conn
        |> put_flash(:info, action)
        |> redirect(to: branch_calendar_path(conn, :index, branch_id))
      {:error, code, resp} ->
        conn
        |> put_flash(:error, "#{code} - #{resp}")
        |> redirect(to: branch_calendar_path(conn, :index, branch_id))
    end
  end

  def render_error_if_exist({:error, code, resp}, conn, event) do
    conn
    |> render(RoomPlz.ChangesetView, "event_error.json", error: "#{code} - #{resp}. Event: #{event.id}")
  end

  def render_error_if_exist(_, _, _) do
  end

  def initiate_event(event, opts \\ %{}) do
    event = Repo.preload(event, [:creator, room: :branch])
    id = if is_integer(event.id), do: @append_to_event_id <> Integer.to_string(event.id), else: event.id

    g_event = %{
      calendar_id: event.room.branch.calendar_id,
      id: id,
      event_id: id,
      summary: event.title,
      start: %{
        "dateTime": "#{DateTime.to_iso8601(event.start)}Z",
        "timeZone": Application.get_env(:room_plz, :timezone_location)
      },
      "end": %{
        "dateTime": "#{DateTime.to_iso8601(event.end)}Z",
        "timeZone": Application.get_env(:room_plz, :timezone_location)
      }
    }

    # Use description field to save event information
    description = %{
      creator: event.creator.name,
      color: event.color,
      creator_id: event.creator.id,
      repeat: event.repeat,
      collaborators: get_collaborator_ids(event.collaborators),
      team_id: event.team_id,
      start_date: ecto_to_local_datetime(event.start, :date_str)
    }

    # Recurrence
    recurrence_rule = get_recurrence_rule(event)

    # Email Invitation
    attendees =
      if event.invitation do
        event.invitation
        |> String.trim(",")
        |> String.split(",")
        |> Enum.map(fn email -> String.trim(email) end)
        |> Enum.map(fn email -> %{email: email} end)
      else
        []
      end

    g_event
    |> Map.merge(%{description: Poison.encode!(description)})
    |> Map.merge(%{recurrence: [recurrence_rule]})
    |> Map.merge(%{attendees: attendees})
    |> Map.merge(opts)
  end

  defp get_collaborator_ids(nil) do
    nil
  end

  defp get_collaborator_ids(collaborators) do
    collaborators
    |> String.split(",")
    |> Enum.map(&get_collaborator_id(&1))
  end

  defp get_collaborator_id(collaborator_email) do
    collaborator = Repo.get_by(User, %{email: collaborator_email})

    collaborator.id
  end

  def get_recurrence_rule(%{repeat: repeat} = event) when not is_nil(repeat) do
    frequence = ["RRULE:FREQ", String.upcase(event.frequency)]
    interval = ["INTERVAL", Integer.to_string(event.interval)]

    limit =
      cond do
        event.count -> ["COUNT", Integer.to_string(event.count)]
        event.until ->
          until =
            ~r/-/
            |> Regex.replace(Ecto.Date.to_string(event.until), "", global: true)
            |> Kernel.<>("T235959Z")
          ["UNTIL", until]
        true -> []
      end

    by_day =
      if (event.frequency == "Weekly" || event.frequency == "Monthly") && event.weekday do
        # remove the last "," in event.weekday string
        ["BYDAY", String.trim_trailing(event.weekday, ",")]
      else
        []
      end

    [frequence, limit, interval, by_day]
    |> Enum.filter(fn x -> Enum.count(x) > 0 end)
    |> Enum.map(fn x -> Enum.join(x, "=") end)
    |> Enum.join(";")
  end

  def get_recurrence_rule(_) do
    nil
  end

  def add_exception_rule(g_event, event) do
    time_str =
      event.start
      |> Ecto.DateTime.to_time()
      |> Ecto.Time.to_string()

    date_except_rule =
      event.date_exception
      |> Enum.map(fn date -> Regex.replace(~r/(-|:)/, "#{date}T#{time_str}Z", "") end)
      |> Enum.join(",")

    new_recurrence_rule = Enum.concat(g_event.recurrence, ["EXDATE:#{date_except_rule}"])

    Map.merge(g_event, %{recurrence: new_recurrence_rule})
  end

  def get_teams(conn, _params) do
    teams =
      Team
      |> Repo.all
      |> ModelHelpers.options_for_select(:name, :id)

    assign(conn, :teams, teams)
  end
end
