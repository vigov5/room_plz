defmodule RoomPlz.EmailController do
  use Phoenix.Swoosh, view: Coherence.EmailView, layout: {Coherence.LayoutView, :email}
  alias Swoosh.Email
  alias RoomPlz.Coherence.Mailer

  def send_password(user) do
    from_name = Application.get_env(:coherence, :email_from_name)
    from_email = Application.get_env(:coherence, :email_from_email)

    %Email{}
    |> to({user["name"], user["email"]})
    |> from({from_name, from_email})
    |> subject("RoomPlz - Retrieve your account information")
    |> render_body("account_info.html", %{user: user})
    |> Mailer.deliver
  end
end