defmodule RoomPlz.EventController do
  use RoomPlz.Web, :controller
  import RoomPlz.Endpoint
  alias GoogleCalendar.Event, as: GEvent
  alias RoomPlz.{Event, Branch, EventLog, EventLogController, ModelHelpers}

  plug :scrub_params, "event" when action in [:create, :update]
  plug :load_and_authorize_resource, model: Event, only: [:index, :show, :edit, :update, :delete, :new, :create], preload: [:creator, :team, room: :branch]
  plug :load_resource, model: Branch, only: [:show, :new, :create, :edit, :update, :delete], id_name: "branch_id", persisted: true
  plug :get_client when action in [:create, :update]
  plug :get_teams when action in [:new, :create, :edit, :update]

  @append_to_event_id Application.get_env(:room_plz, :append_to_event_id)
  @opts [params: [sendNotifications: true]]

  def index(conn, %{"branch_id" => branch_id} = params) do
    branch = Repo.get!(Branch, branch_id)
    events =
      Event
      |> Event.with_room_and_branch
      |> Event.from_branch(branch)
      |> ModelHelpers.sort_by_id()
      |> ModelHelpers.with_limit(params)
      |> Repo.all()
      |> Repo.preload([:room, :creator, :team])

    branches = Branch.all_for_select()

    render(conn, "index.html", events: events, branches: branches, branch: branch)
  end

  def new(conn, _params) do
    changeset = Event.changeset(%Event{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"event" => event_params}) do
    branch_id = conn.assigns.branch.id
    changeset =
      %Event{}
      |> Event.changeset(event_params)
      |> add_exceptions(conn)

    case Repo.insert(changeset) do
      {:ok, event} ->
        if !Map.has_key?(changeset, :overlap_events) do
          # create log
          event
          |> Repo.preload([room: :branch])
          |> initiate_log(conn, event_params["action"])
        end

        g_event = initiate_event(event)

        conn.assigns.client
        |> GEvent.insert(g_event, @opts)
        |> refetch_and_show_resp(conn, branch_id)

      {:error, changeset} ->
        if conn.params["event"]["form_type"] do
          render(conn, RoomPlz.ChangesetView, "error.json", changeset: changeset)
        else
          render(conn, "new.html", changeset: changeset)
        end
    end
  end

  def show(conn, _params) do
    event_logs =
      EventLog
      |> EventLog.from_event(conn.assigns.event)
      |> Repo.all()
      |> Repo.preload([:event, :user])

    render(conn, "show.html", event_logs: event_logs)
  end

  def edit(conn, _params) do
    changeset = Event.changeset(conn.assigns.event)
    render(conn, "edit.html", changeset: changeset)
  end

  # Update event in form
  def update(conn, %{"event" => %{"title" => _title} = event_params}) do
    branch_id = conn.assigns.branch.id
    changeset = Event.changeset(conn.assigns.event, event_params)

    case Repo.update(changeset) do
      {:ok, event} ->
        event
        |> Repo.preload([room: :branch])
        |> create_log_and_update_event(conn, branch_id)
      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end

  # Update event by dragging or dropping on calendar
  def update(conn, %{"event" =>  %{"start_time" => start_time, "end_time" => end_time}}) do
    branch_id = conn.assigns.branch.id
    start_date = ecto_to_local_datetime(conn.assigns.event.start, :date_str)
    start_time = "#{start_date}T#{start_time}Z"
    end_time = "#{start_date}T#{end_time}Z"

    changeset =
      conn.assigns.event
      |> Event.changeset(%{start: date_str_to_datetime(start_time, :utc), "end": date_str_to_datetime(end_time, :utc)})

    if changeset.valid? do
      event =
        changeset
        |> Repo.update!()
        |> Repo.preload([:creator, room: :branch])

      create_log_and_update_event(event, conn, branch_id)
    else
      render(conn, RoomPlz.ChangesetView, "error.json", changeset: changeset)
    end
  end

  # Update event status
  def update(conn, %{"id" => id, "event" =>  %{"status" => updated_status}}) do
    branch_id = conn.assigns.branch.id
    client = conn.assigns.client
    event = conn.assigns.event

    event
    |> Event.changeset(%{status: updated_status})
    |> Repo.update!

    detail = "Change status from #{event.status} to #{updated_status}"
    create_log(conn, event, updated_status, detail)

    id = @append_to_event_id <> id

    if updated_status == "rejected" do
      client
      |> GEvent.delete(%{calendar_id: event.room.branch.calendar_id, id: id}, @opts)
      |> refetch_and_show_resp(conn, branch_id)
    else
      g_event = initiate_event(event, %{status: "confirmed"})

      client
      |> GEvent.update(g_event, @opts)
      |> refetch_and_show_resp(conn, branch_id)
    end
  end

  # Reject single event recurrence
  def update(conn, %{"event" => %{"except-time" => except_time}}) do
    {_, except_time, _} = DateTime.from_iso8601(except_time)

    except_date =
      except_time
      |> DateTime.to_date()
      |> Date.to_string()

    conn.assigns.event
    |> Map.merge(%{except_date: except_date})
    |> add_exception(conn)
    |> refetch_and_show_resp(conn, conn.assigns.branch.id)
  end

  def delete(conn, _params) do
    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(conn.assigns.event)

    conn
    |> put_flash(:info, "Event deleted successfully.")
    |> redirect(to: branch_event_path(conn, :index, conn.assigns.branch))
  end

  defp create_log_and_update_event(event, conn, branch_id) do
    action = "updated event"

    # create log
    initiate_log(event, conn, action)

    # update event
    g_event = initiate_event(event)

    conn.assigns.client
    |> GEvent.update(g_event, @opts)
    |> refetch_and_show_resp(conn, branch_id)
  end

  def initiate_log(event, conn, action) do
    detail = initiate_detail(event)
    create_log(conn, event, action, detail)
  end

  defp initiate_detail(event) do
    date = ecto_to_local_datetime(event.start, :date_str)
    start_time = ecto_to_local_datetime(event.start, :shorten_time_str)
    end_time = ecto_to_local_datetime(event.end, :shorten_time_str)
    repeat = if event.repeat, do: ", repeat rules: #{event.repeat}", else: ""

    "Start date: #{date}, from #{start_time} to #{end_time <> repeat}"
  end

  def create_log(conn, event, action, detail) do
    log = %{
      action: String.upcase(action),
      detail: detail,
      user_id: conn.assigns.current_user.id,
      branch_id: event.room.branch.id,
      event_id: event.id
    }
    EventLogController.create(log)
  end

  def send_notification(conn, event, action) do
    date = ecto_to_local_datetime(event.start, :date_str)
    href = branch_calendar_url(conn, :index, conn.assigns.branch.id, date: date)
    event = Repo.preload(event, [:creator, room: :branch])
    # notification
    broadcast!("admins", "notification", %{event: RoomPlz.APIView.render("event.json", %{event: event}), action: action, href: href})
  end

  defp refetch_and_show_resp(result, conn, branch_id) do
    broadcast!("users", "refetch", %{})
    show_resp(result, conn, branch_id)
  end

  # Add exception date
  defp add_exceptions(%{changes: %{overlap_events: overlap_events}} = changeset, conn) do
    overlap_events
    |> Enum.map(fn event -> Map.merge(event, %{except_date: event.same_date}) end)
    |> Enum.each(&add_exception(&1, conn))

    changeset
  end

  defp add_exceptions(changeset, _) do
    changeset
  end

  defp add_exception(event, conn) do
    detail = "Add exception on #{event.except_date}"
    action = "Exclude date"
    event = Repo.preload(event, [room: :branch])

    # Create Log
    create_log(conn, event, action, detail)

    # Update local event
    updated_event =
      event
      |> Event.update_exception_date()
      |> Repo.update!()

    # Exclude exception date in google repeate event/ delete single event
    g_event = initiate_event(event)
    if event.repeat == nil do
      id = @append_to_event_id <> Integer.to_string(event.id)
      GEvent.delete(conn.assigns.client, %{calendar_id: event.room.branch.calendar_id, id: id}, @opts)
    else
      g_event = add_exception_rule(g_event, updated_event)

      GEvent.update(conn.assigns.client, g_event, @opts)
    end
  end
end
