defmodule RoomPlz.SlotController do
  use RoomPlz.Web, :controller
  alias RoomPlz.{Slot, SlotLogController, Email}

  def index(conn, _params) do
    slots =
      Slot
      |> Repo.all()
      |> Repo.preload([:creator, :car])

    render(conn, "index.html", slots: slots)
  end

  def new(conn, _params) do
    changeset = Slot.changeset(%Slot{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"slot" => slot_params}) do
    slot_params = convert_slot_time(slot_params)
    changeset = Slot.changeset(%Slot{}, slot_params)

    case Repo.insert(changeset) do
      {:ok, slot} ->
        create_log(slot, conn, slot_params["action"])
        send_email(slot, "created")

        render(conn, RoomPlz.ChangesetView, "event_success.json", success: "Create slot successfully")
      {:error, changeset} ->
        render(conn, RoomPlz.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    slot =
      Slot
      |> Repo.get!(id)
      |> Repo.preload([:creator, :car, slot_logs: :user])

    render(conn, "show.html", slot: slot)
  end

  def edit(conn, %{"id" => id}) do
    slot = Repo.get!(Slot, id)
    changeset = Slot.changeset(slot)
    render(conn, "edit.html", slot: slot, changeset: changeset)
  end

  def update(conn, %{"id" => id, "slot" => %{"start_time" => _} = slot_params}) do
    slot_params = convert_slot_time(slot_params)

    changeset =
      Slot
      |> Repo.get!(id)
      |> Slot.changeset(slot_params)

    case Repo.update(changeset) do
      {:ok, slot} ->
        create_log(slot, conn, slot_params["action"])
        send_email(slot, "updated")

        render(conn, RoomPlz.ChangesetView, "event_success.json", success: "Update slot successfully")
      {:error, changeset} ->
        render(conn, RoomPlz.ChangesetView, "error.json", changeset: changeset)
    end
  end

  # Update event status
  def update(conn, %{"id" => id, "slot" =>  %{"status" => updated_status} = params}) do
    slot = Repo.get!(Slot, id)

    slot
    |> Slot.changeset(%{status: updated_status})
    |> Repo.update!
    |> send_email("updated")

    create_status_log(conn, slot, updated_status)


    if params["form_type"] == "form" do
      conn
      |> put_flash(:info, "The slot was #{updated_status}")
      |> redirect(to: car_path(conn, :show_car_calendar))
    else
      render(conn, RoomPlz.ChangesetView, "event_success.json", success: "#{String.capitalize(updated_status)} slot successfully")
    end
  end

  def delete(conn, %{"id" => id}) do
    slot = Repo.get!(Slot, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(slot)

    conn
    |> put_flash(:info, "Slot deleted successfully.")
    |> redirect(to: slot_path(conn, :index))
  end

  def create_log(slot, conn, action) do
    date = ecto_to_local_datetime(slot.start, :date_str)
    start_time = ecto_to_local_datetime(slot.start, :shorten_time_str)
    end_time = ecto_to_local_datetime(slot.end, :shorten_time_str)

    detail = "#{date}: from #{start_time} to #{end_time}"
    slot = %{
      action: String.upcase(action),
      detail: detail,
      utilizer: slot.utilizer,
      driver: slot.driver,
      user_id: conn.assigns.current_user.id,
      slot_id: slot.id
    }

    SlotLogController.create(slot)
  end

  def create_status_log(conn, slot, updated_status) do
    log = %{
      action: String.upcase(updated_status),
      detail: "Change status from #{slot.status} to #{updated_status}",
      utilizer: slot.utilizer,
      driver: slot.driver,
      slot_id: slot.id,
      user_id: conn.assigns.current_user.id
    }
    SlotLogController.create(log)
  end

  def send_email(slot, action) do
    if slot.invitation do
      slot.invitation
      |> String.split(",")
      |> Enum.each(&Email.send_notification_email(&1, slot, action))
    end
  end

  def convert_slot_time(params) do
    Map.merge(params, %{"start" => date_str_to_datetime(params["start_time"], :utc),
                        "end" => date_str_to_datetime(params["end_time"], :utc)})
  end
end
