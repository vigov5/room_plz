defmodule RoomPlz.PageController do
  use RoomPlz.Web, :controller
  alias RoomPlz.Branch

  def index(conn, _params) do
    branches = Branch.all_for_select(conn.assigns.current_user)
    render conn, "index.html", branches: branches
  end
end
