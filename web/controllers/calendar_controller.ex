defmodule RoomPlz.CalendarController do
  use RoomPlz.Web, :controller
  alias RoomPlz.{Branch, Room}

  plug :scrub_get_params
  plug :before_filter
  plug :get_teams when action in [:index]

  def index(conn, %{"branch_id" => branch_id}) do
    branch = Repo.get!(Branch, branch_id)
    rooms =
      Room
      |> Room.from_branch(branch)
      |> Repo.all()

    render(conn, "index.html", rooms: rooms, branch: branch)
  end

  def before_filter(conn, _params) do
    branch = Repo.get!(Branch, conn.params["branch_id"])
    is_logged_in = Coherence.logged_in?(conn)

    conn
    |> assign(:branch, branch)
    |> assign(:selectable, is_logged_in)
    |> assign(:editable, is_logged_in)
  end
end
