defmodule RoomPlz.SlotLogController do
  use RoomPlz.Web, :controller
  import RoomPlz.ModelHelpers
  alias RoomPlz.SlotLog

  def index(conn, params) do
    slot_logs =
      SlotLog
      |> with_limit(params)
      |> Repo.all()
      |> Repo.preload([:user, :slot])
    render(conn, "index.html", slot_logs: slot_logs)
  end

  def create(params) do
    changeset = SlotLog.changeset(%SlotLog{}, params)
    Repo.insert!(changeset)
  end
end
