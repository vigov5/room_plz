defmodule RoomPlz.APIView do
  use RoomPlz.Web, :view
  import RoomPlz.Tools

  def render("events.json", %{events: events}) do
    render_many(events, RoomPlz.APIView, "event.json", as: :event)
  end

  def render("event.json", %{event: event}) do
    %{
      id: event.id,
      status: event.status,
      title: event.title,
      description: event.description,
      start: ecto_to_local_datetime(event.start),
      end: ecto_to_local_datetime(event.end),
      repeat: event.repeat,
      frequency: event.frequency,
      interval: event.interval,
      weekday: event.weekday,
      count: event.count,
      until: event.until,
      color: event.room.color,
      room: %{
        room_id: event.room.id,
        color: event.room.color,
        name: event.room.name,
        branch: %{
          branch_id: event.room.branch.id,
          name: event.room.branch.name
        }
      },
      creator: event.creator.name,
      creator_id: event.creator.id
    }
  end

  def render("slots.json", %{slots: slots}) do
    render_many(slots, RoomPlz.APIView, "slot.json", as: :slot)
  end

  def render("slot.json", %{slot: slot}) do
    %{
      id: slot.id,
      status: slot.status,
      title: slot.title,
      description: slot.description,
      utilizer: slot.utilizer,
      driver: slot.driver,
      invitation: slot.invitation,
      start: ecto_to_local_datetime(slot.start),
      end: ecto_to_local_datetime(slot.end),
      color: slot.car.color,
      car: %{
        car_id: slot.car.id,
        color: slot.car.color,
        name: slot.car.name,
        description: slot.car.description
      },
      creator: slot.creator.name,
      creator_id: slot.creator.id
    }
  end

  def render("dates_with_rooms.json", %{dates: dates}) do
    render_many(dates, RoomPlz.APIView, "date_with_rooms.json", as: :date)
  end

  def render("date_with_rooms.json", %{date: {date, rooms}}) do
    %{
      date: date,
      rooms: render("rooms.json", %{rooms: rooms})
    }
  end

  def render("rooms.json", %{rooms: rooms}) do
    render_many(rooms, RoomPlz.APIView, "room.json", as: :room)
  end

  def render("room.json", %{room: {room, times}}) do
    %{
      id: room.id,
      color: room.color,
      name: room.name,
      capacity: room.capacity,
      times: render_many(times, RoomPlz.APIView, "time.json", as: :time)
    }
  end

  def render("room.json", %{room: room}) do
    %{
      id: room.id,
      color: room.color,
      name: room.name,
      capacity: room.capacity,
      overlap_acceptance: room.overlap_acceptance
    }
  end

  def render("cars.json", %{cars: cars}) do
    render_many(cars, RoomPlz.APIView, "car.json", as: :car)
  end

  def render("car.json", %{car: car}) do
    %{
      id: car.id,
      color: car.color,
      name: car.name,
      capacity: car.capacity,
      description: car.description
    }
  end

  def render("time.json", %{time: {start_time, end_time}}) do
    %{
      start_time: date_str_to_datetime(start_time, :local_time_str),
      end_time: date_str_to_datetime(end_time, :local_time_str)
    }
  end

  def render("branch.json", %{branch: branch}) do
    %{
      id: branch.id,
      name: branch.name,
      calendar_id: branch.calendar_id,
      rooms: render("rooms.json", %{rooms: branch.rooms})
    }
  end

  def render("log.json", %{log: log}) do
    %{
      action: log.action,
      detail: log.detail,
      event: log.event.title,
      user: log.user.name,
    }
  end

  def render("email_list.json", %{email_list: email_list}) do
    render_many(email_list, RoomPlz.APIView, "item.json", as: :item)
  end

  def render("item.json", %{item: item}) do
    %{
      email: item.email
    }
  end
end
