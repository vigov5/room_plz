defmodule RoomPlz.EventView do
  use RoomPlz.Web, :view
  use Timex

  def convert_timezone(datetime) do
    RoomPlz.Tools.ecto_to_local_datetime(datetime, :datetime_str)
  end

  def get_time(datetime) do
    RoomPlz.Tools.ecto_to_local_datetime(datetime, :time_str)
  end
end