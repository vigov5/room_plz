defmodule RoomPlz.ChangesetView do
  use RoomPlz.Web, :view
  alias Ecto.Changeset

  @doc """
  Traverses and translates changeset errors.

  See `Ecto.Changeset.traverse_errors/2` and
  `RoomPlz.ErrorHelpers.translate_error/1` for more details.
  """
  def translate_errors(changeset) do
    Changeset.traverse_errors(changeset, &translate_error/1)
  end

  def render("error.json", %{changeset: changeset}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    %{errors: translate_errors(changeset)}
  end

  def render("event_error.json", %{error: error}) do
    %{error: error}
  end

  def render("event_success.json", %{success: success}) do
    %{success: success}
  end
end
