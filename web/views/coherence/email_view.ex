defmodule Coherence.EmailView do
  use RoomPlz.Coherence.Web, :view
  use Timex

  def get_date(datetime) do
    RoomPlz.Tools.ecto_to_local_datetime(datetime, :date_str)
  end

  def get_time(datetime) do
    RoomPlz.Tools.ecto_to_local_datetime(datetime, :time_str)
  end
end
