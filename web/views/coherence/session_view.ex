defmodule Coherence.SessionView do
  use RoomPlz.Coherence.Web, :view

  def render("new.json", _conn) do
    %{
      error: "Please login to do that"
    }
  end
end
