defmodule RoomPlz.AdminChannel do
  use RoomPlz.Web, :channel

  def join("admins", _params, socket) do

    {:ok, %{}, socket}
  end
end