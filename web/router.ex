defmodule RoomPlz.Router do
  use RoomPlz.Web, :router
  use Coherence.Router
  alias RoomPlz.Branch

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Coherence.Authentication.Session
    plug :get_other_branches
  end

  pipeline :protected do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Coherence.Authentication.Session, protected: true
    plug :get_other_branches
  end

  scope "/" do
    pipe_through :browser
    coherence_routes
  end

  scope "/" do
    pipe_through :protected
    coherence_routes :protected
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug Coherence.Authentication.Session
  end

  pipeline :api_protected do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
    plug Coherence.Authentication.Session, protected: true
  end

  scope "/", RoomPlz do
    pipe_through :protected # protected resources

    resources "/users", UserController do
      get "/requests", UserController, :user_requests
    end

    resources "/branches", BranchController do
      resources "/events", EventController
      resources "/rooms", RoomController
    end
    get "/event_logs", EventLogController, :index

    # Car Booking
    resources "/cars", CarController
    get "/car_logs", CarController, :show_car_logs

    resources "/slots", SlotController, except: [:show]
    get "/slot_logs", SlotLogController, :index

    # Authorization
    get "/auth/google", AuthController, :gg_authenticate
    get "/auth/google/callback", AuthController, :gg_callback
    delete "/auth/google", AuthController, :gg_delete

  end

  scope "/", RoomPlz do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/teams", TeamController
    get"/slots/:id", SlotController,  :show
    get "/rooms/suggestion", RoomController, :suggestion

    resources "/branches", BranchController, only: [:show] do
      get "/calendar", CalendarController, :index
    end
    # Car Booking
    get "/car_calendar", CarController, :show_car_calendar

    # Authorization
    get "/auth/wsm", AuthController, :wsm_authenticate
    get "/auth/wsm/callback", AuthController, :wsm_callback
  end

  scope "/api", RoomPlz do
    pipe_through :api

    get "/rooms", APIController, :rooms
    get "/branches/:id", APIController, :show_branch
    get "/suggestion", APIController, :suggestion

    # Car Booking
    get "/cars", APIController, :cars
    get "/slots", APIController, :slots
    get "/slot/:id", APIController, :get_slot
  end

  scope "/api", RoomPlz do
    pipe_through :api_protected

    get "/users", APIController, :users
    get "/users/:id", APIController, :get_invitation_list
    post "/events/synchronize", APIController, :synchronize
    post "/import_events", APIController, :import_events
  end

  defp get_other_branches(conn, _params) do
    branch_id = if conn.params["branch_id"], do: String.to_integer(conn.params["branch_id"])
    assign(conn, :other_branches, Branch.get_other_branches(branch_id))
  end
end
