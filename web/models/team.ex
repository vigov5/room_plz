defmodule RoomPlz.Team do
  use RoomPlz.Web, :model

  schema "teams" do
    field :name, :string

    has_many :events, RoomPlz.Event

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
