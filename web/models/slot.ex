defmodule RoomPlz.Slot do
  use RoomPlz.Web, :model
  import RoomPlz.{ModelHelpers, EventView}
  alias RoomPlz.Car

  schema "slots" do
    field :title, :string
    field :start, Ecto.DateTime
    field :end, Ecto.DateTime
    field :status, :string
    field :description, :string
    field :utilizer, :string
    field :driver, :string
    field :invitation, :string

    belongs_to :creator, RoomPlz.User
    belongs_to :car, RoomPlz.Car
    has_many :slot_logs, RoomPlz.SlotLog
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :start, :end, :status, :description, :utilizer, :driver, :invitation, :creator_id, :car_id])
    |> validate_required([:title, :start, :end, :status, :utilizer, :driver, :creator_id, :car_id])
    |> validate_duration
    |> check_available
    |> change_title
  end

  defp validate_duration(changeset) do
    start_time = get_field(changeset, :start)
    end_time = get_field(changeset, :end)

    case Ecto.DateTime.compare(start_time, end_time) do
      :lt -> changeset
      _ -> add_error(changeset, :duration, "Start time must before end time")
    end
  end

  defp check_available(%{:valid? => true} = changeset) do
    start_time = get_field(changeset, :start)
    end_time = get_field(changeset, :end)
    car_id = get_field(changeset, :car_id)
    id = get_field(changeset, :id)

    overlap_slots =
      RoomPlz.Slot
      |> same_car_id(car_id)
      |> not_id(id)
      |> is_status("approved")
      |> is_overlapped(start_time, end_time)
      |> Repo.all

    case overlap_slots do
      [head | _tail] ->
        add_error(changeset, :overlap, "Overlap with one or more slots,
          for example: #{head.title} on #{Map.get(head, "same_date")}
          from #{get_time(head.start)} ~ #{get_time(head.end)}")
      [] ->
        changeset
    end
  end

  defp check_available(%{:valid? => false} = changeset) do
    changeset
  end

  def same_car_id(query, car_id) do
    from s in query,
         where: s.car_id == ^car_id
  end

  defp change_title(%{:valid? => true} = changeset) do
    car_id = get_field(changeset, :car_id)
    title = get_field(changeset, :title)
    car = Repo.get!(Car, car_id)
    added_car = "[#{car.name}] "
    new_title = added_car <> title

    if String.contains?(title, added_car) do
      changeset
    else
      put_change(changeset, :title, new_title)
    end
  end

  defp change_title(%{:valid? => false} = changeset) do
    changeset
  end

  # Change slot title when car change name
  def change_all_titles(car, new_car_name) do
    RoomPlz.Slot
    |> with_car
    |> from_car(car)
    |> Repo.all
    |> Enum.each(&change_slot_title(&1, new_car_name))
  end

  defp change_slot_title(slot, new_car_name) do
    new_title =
      ~r/\[(.*)\]/
      |> Regex.replace(slot.title, "[#{new_car_name}]")

    slot
    |> cast(%{"title" => new_title}, [:title])
    |> Repo.update()
  end

  def with_car(query) do
    query
    |> join(:inner, [s], c in assoc(s, :car))
  end

  def from_car(query, car) do
    query
    |> where([s, c], c.id == ^car.id)
  end
end
