defmodule RoomPlz.SlotLog do
  use RoomPlz.Web, :model

  schema "slot_logs" do
    field :action, :string
    field :detail, :string
    field :utilizer, :string
    field :driver, :string
    belongs_to :user, RoomPlz.User
    belongs_to :slot, RoomPlz.Slot

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:action, :detail, :utilizer, :driver, :user_id, :slot_id])
    |> validate_required([:action, :detail, :utilizer, :user_id, :slot_id])
  end
end
