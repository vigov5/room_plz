defmodule RoomPlz.Room do
  use RoomPlz.Web, :model
  alias RoomPlz.Event

  schema "rooms" do
    field :name, :string
    field :capacity, :integer
    field :color, :string
    field :description, :string
    field :overlap_acceptance, :boolean

    belongs_to :branch, RoomPlz.Branch
    has_many :events, RoomPlz.Event

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @repeat_type [{"Daily", "Daily"}, {"Weekly", "Weekly"}, {"Monthly", "Monthly"}, {"Yearly", "Yearly"}]
  @week_order [{"First", 1}, {"Second", 2}, {"Third", 3}, {"Forth", 4}, {"Fifth", 5}]

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :capacity, :color, :branch_id, :description, :overlap_acceptance])
    |> validate_required([:name, :capacity, :color, :branch_id])
    |> unique_constraint(:name_constraint, name: :name_in_branch_index)
    |> check_name_change(struct, params)
  end

  def from_branch(query, branch) do
    from r in query,
    where: r.branch_id == ^branch.id
  end

  def asc_by_name(query) do
    from r in query,
    order_by: [r.name == "Other", r.name]
  end

  def all_for_branch(branch_id) do
    query = from r in RoomPlz.Room,
            where: r.branch_id == ^branch_id
    query
    |> asc_by_name()
    |> Repo.all()
    |> ModelHelpers.options_for_select(:name, :id)
  end

  def get_repeat_type(:type), do: @repeat_type
  def get_repeat_type(:order), do: @week_order

  def get_repeat_time(array, n) when n == 0 do
    Enum.sort(array)
  end

  def get_repeat_time(array, n) do
    array = array ++ [{n, n}]
    get_repeat_time(array, n - 1)
  end

  defp check_name_change(%{valid?: true} = changeset, %{name: old_name} = struct,
                         %{"name" => new_name} = params)
                         when not is_nil(old_name) and old_name != new_name do

    Event.change_all_titles(struct, params)
    changeset
  end

  defp check_name_change(changeset, _, _) do
    changeset
  end
end
