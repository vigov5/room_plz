defmodule RoomPlz.Branch do
  use RoomPlz.Web, :model
  alias RoomPlz.{ModelHelpers, Room, Branch}

  schema "branches" do
    field :name, :string
    field :calendar_id, :string
    field :auth_email, :string
    field :refresh_token, :string

    has_many :rooms, Room
    has_many :event_logs, RoomPlz.EventLog

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :calendar_id, :auth_email, :refresh_token])
    |> validate_required([:name, :calendar_id])
  end

  defp except_the_current(query, nil) do
    query
  end

  defp except_the_current(query, branch_id) do
    from b in query,
    where: b.id != ^branch_id
  end

  defp sort_by_name(query) do
    from b in query,
    order_by: b.name
  end

  defp sort_default_branch(query, %{branch_id: branch_id} = _user) when not is_nil(branch_id) do
    from b in query,
    order_by: [desc: b.id == ^branch_id]
  end

  defp sort_default_branch(query, user) when is_nil(user) or is_map(user) do
    query
  end

  def all_for_select(user \\ %{}) do
    Branch
    |> sort_default_branch(user)
    |> sort_by_name()
    |> Repo.all
    |> ModelHelpers.options_for_select(:name, :id)
  end

  def default_branch_id(user \\ %{}) do
    default_branch_id =
      user
      |> Branch.all_for_select()
      |> Enum.map(&(elem(&1, 1)))
      |> List.first

    default_branch_id || 0
  end

  def get_other_branches(branch_id) do
    Branch
    |> except_the_current(branch_id)
    |> sort_by_name()
    |> Repo.all
    |> Enum.map(fn event -> %{id: event.id, name: event.name} end)
  end

  def get_link(conn, branch_id) do
    regex = ~r/\/branches\/(.*?)\//

    Regex.replace(regex, conn.request_path, "/branches/#{branch_id}/")
  end
end
