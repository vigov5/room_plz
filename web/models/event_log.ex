defmodule RoomPlz.EventLog do
  use RoomPlz.Web, :model

  schema "event_logs" do
    field :action, :string
    field :detail, :string
    belongs_to :user, RoomPlz.User
    belongs_to :branch, RoomPlz.Branch
    belongs_to :event, RoomPlz.Event

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:action, :detail, :user_id, :branch_id, :event_id])
    |> validate_required([:action, :detail])
  end

  def from_branch(query, branch) do
    from l in query,
    where: l.branch_id == ^branch.id
  end

  def from_event(query, event) do
    from l in query,
    where: l.event_id == ^event.id
  end
end
