defmodule RoomPlz.ModelHelpers do
  use RoomPlz.Web, :model

  @limit 150 # Default event limit
  def options_for_select(records, text, value) do
    records
    |> Enum.map(fn b -> {Map.get(b, text), Map.get(b, value)} end)
  end

  def to_datetime(date) do
    date
    |> Timex.parse!("{YYYY}-{0M}-{0D}")
    |> Ecto.DateTime.cast
  end

  def in_range(query, start_time, end_time) do
    from e in query,
         where: e.start >= ^start_time and ^end_time >= e.end
  end

  def is_overlapped(query, start_time, end_time) do
    from e in query,
         where: e.start <= ^end_time and ^start_time <= e.end
  end

  def from_date(query, date_time) do
    from e in query,
         where: e.end > ^date_time
  end

  def is_status(query, status) do
    from e in query,
         where: e.status == ^status
  end

  def is_not_status(query, status) do
    from e in query,
         where: e.status != ^status
  end

  def not_rejected(query) do
    from e in query,
         where: e.status != "rejected"
  end

  def not_id(query, nil) do
    query
  end

  def not_id(query, id) do
    from e in query,
         where: e.id != ^id
  end

  def with_limit(query, params \\ %{})

  def with_limit(query, %{"show" => "all"}) do
    query
  end

  def with_limit(query, _params) do
    from e in query, limit: @limit
  end

  def sort_by_id(query) do
    from e in query, order_by: [desc: e.id]
  end

  def sort_by_email(query) do
    from e in query, order_by: [asc: e.email]
  end
end
