defmodule RoomPlz.Car do
  use RoomPlz.Web, :model
  alias RoomPlz.Slot

  schema "cars" do
    field :name, :string
    field :capacity, :integer
    field :description, :string
    field :color, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :capacity, :description, :color])
    |> validate_required([:name, :capacity, :color])
    |> unique_constraint(:name, name: :car_name_index)
    |> check_name_change(struct, params)
  end

  def asc_by_name(query) do
    from c in query,
       order_by: :name
  end

  defp check_name_change(%{valid?: true} = changeset, %{name: old_name} = struct, %{"name" => new_name}) when old_name != nil do
    Slot.change_all_titles(struct, new_name)

    changeset
  end

  defp check_name_change(changeset, _struct, _params) do
    changeset
  end
end
