defmodule RoomPlz.User do
  use RoomPlz.Web, :model
  use Coherence.Schema

  schema "users" do
    field :name, :string
    field :email, :string
    field :role, :string
    field :invitation_list, :string
    field :team_id, :integer
    coherence_schema

    has_many :events, RoomPlz.Event
    has_many :event_logs, RoomPlz.EventLog
    belongs_to :branch, RoomPlz.Branch

    timestamps
  end

  def changeset(model, params \\ %{}) do
    params = add_default_role(model, params)

    model
    |> cast(params, [:name, :email, :role, :branch_id, :invitation_list, :team_id] ++ coherence_fields)
    |> validate_required([:name, :email])
    |> validate_format(:email, ~r/@framgia.com/, [message: "requires Framgia email"])
    |> unique_constraint(:email)
    |> validate_coherence(params)
  end

  def changeset(model, params, :password) do
    model
    |> cast(params, [:password, :password_confirmation, :reset_password_token, :reset_password_sent_at])
    |> validate_coherence_password_reset(params)
  end

  defp add_default_role(_model, %{"role" => _role} = params) do
    params
  end

  defp add_default_role(model, params) when model == %RoomPlz.User{} do
    Map.merge(params, %{"role" => "member"})
  end

  defp add_default_role(_model, params) do
    params
  end

  def is_admin?(model) do
    model && (model.role == "admin" || model.role == "super-admin")
  end

  def is_super_admin?(model) do
    model && model.role == "super-admin"
  end

  def is_owner(id1, id2) do
    id1 == id2
  end

  def is_collaborator(user_id, collaborators) do
    Enum.any?(collaborators, fn collaborator_id -> user_id == collaborator_id end)
  end

  def all_for_select do
    RoomPlz.User
    |> ModelHelpers.sort_by_email
    |> Repo.all
    |> ModelHelpers.options_for_select(:email, :id)
  end

  def is_edit_page(conn) do
    String.contains?(conn.request_path, "edit")
  end

  def list_roles(user) do
    cond do
      is_super_admin?(user) -> ["member", "admin", "super-admin"]
      is_admin?(user) -> ["member", "admin"]
      true -> ["member"]
    end
  end

  def show_emails(nil) do
    %{}
  end

  def show_emails(lists) do
    lists
    |> String.split(",")
    |> Enum.map(fn email -> Repo.get_by(RoomPlz.User, %{email: email}) end)
    |> Enum.map(fn user -> user.email end)
  end
end
