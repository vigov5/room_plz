defmodule RoomPlz.Event do
  use RoomPlz.Web, :model
  use Timex
  import RoomPlz.{EventView, Tools, ModelHelpers, ControllerHelpers}
  alias RoomPlz.Room
  alias GoogleCalendar.Event, as: GEvent

  schema "events" do
    field :title, :string
    field :start, Ecto.DateTime
    field :end, Ecto.DateTime
    field :status, :string
    field :description, :string
    field :repeat, :string
    field :frequency, :string
    field :interval, :integer
    field :weekday, :string
    field :count, :integer
    field :until, Ecto.Date
    field :date_array, {:array, :string}
    field :is_current, :boolean
    field :date_exception, {:array, :string}
    field :collaborators, :string
    field :invitation, :string
    field :color, :string

    belongs_to :team, RoomPlz.Team
    belongs_to :room, RoomPlz.Room
    belongs_to :creator, RoomPlz.User
    has_many :event_logs, RoomPlz.EventLog

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @weekday %{"MO" => 1, "TU" => 2, "WE" => 3, "TH" => 4, "FR" => 5, "SA" => 6, "SU" => 7}

  # Supporting function
  def same_room_id(query, room_id) do
    from e in query,
         where: e.room_id == ^room_id
  end

  def with_room_and_branch(query) do
    query
    |> join(:inner, [e], r in assoc(e, :room))
    |> join(:inner, [e, r], b in assoc(r, :branch))
  end

  def from_branch(query, branch) do
    query
    |> where([e, r, b], b.id == ^branch.id)
  end

  def from_room(query, room) do
    query
    |> where([e, r, b], r.id == ^room.id)
  end

  def is_current(query) do
    from e in query, where: e.is_current == true
  end

  def get_class(status, type) when type == :approved and status != "approved" do
    "btn btn-success btn-xs approve"
  end

  def get_class(status, type) when type == :rejected and status != "rejected" do
    "btn btn-warning btn-xs reject"
  end

  def get_class(_status, _type) do
    "btn btn-default btn-xs"
  end

  # Changeset Handling
  def changeset(struct, params \\ %{}) do
    params =
      params
      |> check_repeat()
      |> check_start_date()

    struct
    |> cast(params, [:title, :start, :end, :status, :description, :repeat, :frequency, :date_array,
                     :collaborators, :interval, :weekday, :count, :until, :room_id, :creator_id,
                     :is_current, :date_exception, :invitation, :color, :team_id])
    |> validate_required([:title, :start, :end, :status, :room_id, :creator_id])
    |> validate_start_end(params)
    |> change_title
    |> check_slot_reallocation()
  end

  # check repeat
  def check_repeat(%{"repeat" => nil} = params) do
    Map.merge(params, %{"frequency" => nil, "interval" => nil, "count" => nil, "until" => nil})
  end

  def check_repeat(%{"repeat" => _repeat_str, "count" => nil, "until" => nil} = params) do
    params
    |> Map.put("until", nil)
    |> Map.put("count", nil)
  end

  def check_repeat(%{"repeat" => _repeat_str, "count" => count} = params) when not is_nil(count) do
    Map.put(params, "until", nil)
  end

  def check_repeat(%{"repeat" => _repeat_str, "until" => until} = params) when not is_nil(until) do
    Map.put(params, "count", nil)
  end

  def check_repeat(params) do
    params
  end

  # Update start date
  defp check_start_date(%{"weekday" => weekday, "date" => date, "frequency" => "Weekly"} = params) when weekday != nil do
    current_day_of_week =
      date
      |> Date.from_iso8601!()
      |> Date.day_of_week()

    diff_day_of_week =
      weekday
      |> String.trim_trailing(",")
      |> String.split(",")
      |> Enum.map(fn x -> @weekday[x] end)
      |> Enum.map(&diff_index(&1, current_day_of_week))
      |> Enum.min()

    if diff_day_of_week > 0, do: update_start_date(params, diff_day_of_week), else: params
  end

  defp check_start_date(params) do
    params
  end

  defp diff_index(date_index, current_day_of_week) when date_index >= current_day_of_week do
    date_index - current_day_of_week
  end

  defp diff_index(date_index, current_day_of_week) when date_index < current_day_of_week do
    date_index + 7 - current_day_of_week
  end

  defp update_start_date(params, diff) do
    Map.merge(params, %{"start" => move_date(params["start"], diff), "end" => move_date(params["end"], diff)})
  end

  defp move_date(date, diff) do
    {:ok, date_time, _} = DateTime.from_iso8601(date)

    Timex.shift(date_time, days: diff)
  end

  # Ignore validate overlap with imported events
  defp validate_start_end(%{valid?: true} = changeset, %{"is_imported" => true} = _params) do
    {start_time, end_time} = get_duration(changeset)
    {_instance, instance_date_array} = get_instance(changeset, start_time, end_time)

    add_date_array(changeset, instance_date_array)
  end

  defp validate_start_end(%{valid?: true} = changeset, params) when params == %{} do
    changeset
  end

  defp validate_start_end(%{valid?: true} = changeset, params) do
    {start_time, end_time} = get_duration(changeset)

    changeset
    |> validate_start_end(start_time, end_time)
    |> validate_overlap(start_time, end_time, params)
  end

  defp validate_start_end(%{valid?: false} = changeset, _) do
    changeset
  end

  defp validate_start_end(changeset, start_time, end_time) do
    case Ecto.DateTime.compare(start_time, end_time) do
      :lt -> changeset
      _ -> add_error(changeset, :end, "Start time must before end time")
    end
  end

  defp get_duration(changeset) do
    start_time = get_field(changeset, :start)
    end_time = get_field(changeset, :end)

    {start_time, end_time}
  end

  defp change_title(%{valid?: true} = changeset) do
    room_id = get_field(changeset, :room_id)
    title = get_field(changeset, :title)
    room = Repo.get!(Room, room_id)
    added_room = "[#{room.name}] "
    new_title = added_room <> title

    if String.contains?(title, added_room) do
      changeset
    else
      put_change(changeset, :title, new_title)
    end
  end

  defp change_title(%{valid?: false} = changeset) do
    changeset
  end

  # Reset date exception to nil when there is a change in start, end or room
  defp check_slot_reallocation(%{valid?: true} = changeset) do
    start_change = get_change(changeset, :start)
    end_change = get_change(changeset, :end)
    room_change = get_change(changeset, :room_id)

    if start_change || end_change || room_change do
      put_change(changeset, :date_exception, nil)
    else
      changeset
    end
  end

  defp check_slot_reallocation(%{valid?: false} = changeset) do
    changeset
  end

  defp validate_overlap(%{changes: %{status: "rejected"}} = changeset, _, _, _) do
    changeset
  end

  defp validate_overlap(changeset, start_time, end_time, params) do
    room_id = get_field(changeset, :room_id)
    room = Repo.get(Room, room_id)
    if room.overlap_acceptance do
      {_instance, instance_date_array} = get_instance(changeset, start_time, end_time)

      add_date_array(changeset, instance_date_array)
    else
      id = get_field(changeset, :id)

      {instance, instance_date_array} = get_instance(changeset, start_time, end_time)

      overlap_events =
        RoomPlz.Event
        |> same_room_id(room_id)
        |> not_id(id)
        |> not_rejected()
        |> is_current()
        |> Repo.all
        |> Enum.map(&extract_var_and_check_overlap(&1, instance, instance_date_array))
        |> Enum.filter(&Map.has_key?(&1, :same_date))

      case overlap_events do
        [head | _tail] ->
          # Ignore overlapped events if event is inserted forcefully
          if params["force"] do
            changeset
            |> put_change(:overlap_events, overlap_events)
            |> add_date_array(instance_date_array)
          else
            add_error(changeset, :overlap, "Overlap with one or more (repetative) events,
                      for example: #{head.title} on #{Map.get(head, :same_date)}
                      from #{get_time(head.start)} ~ #{get_time(head.end)}")
          end
        [] ->
          add_date_array(changeset, instance_date_array)
      end
    end
  end

  defp add_date_array(changeset, instance_date_array) do
    changeset
    |> put_change(:is_current, true)
    |> put_change(:date_array, instance_date_array)
  end

  defp get_instance(changeset, start_time, end_time) do
    instance = %{:date => ecto_to_local_datetime(start_time, :date),
      :start_time => ecto_to_local_datetime(start_time, :time_str),
      :end_time => ecto_to_local_datetime(end_time, :time_str)}

    instance_date_array =
      if get_field(changeset, :repeat) do
        check_and_create_date_array(Map.merge(changeset.data, changeset.changes), instance)
      else
        create_date_array(instance.date, :days)
      end

    {instance, instance_date_array}
  end

  defp extract_var_and_check_overlap(%{"date_array": target_date_array} = event, instance, instance_date_array) do
    # initialize variable
    target = %{:date => ecto_to_local_datetime(event.start, :date),
               :start_time => ecto_to_local_datetime(event.start, :time_str), :end_time => ecto_to_local_datetime(event.end, :time_str)}
    # check overlap
    check_date_and_time_overlap(event, instance_date_array, target_date_array, instance, target)
  end

  def check_and_create_date_array(event, time) do
    until = get_until(event)
    case event.frequency do
      "Daily" -> create_date_array(time.date, :days, event.interval, until)
      "Weekly" ->
        if Map.has_key?(event, :weekday) && event.weekday do
          create_date_array(time.date, :weeks, event.interval, until, event.weekday)
        else
          create_date_array(time.date, :weeks, event.interval, until)
        end
      "Monthly" ->
        if Map.has_key?(event, :weekday) && event.weekday do
          create_date_array(Timex.beginning_of_month(time.date), :months, event.interval, until, event.weekday)
        else
          create_date_array(time.date, :months, event.interval, until)
        end
      "Yearly" -> create_date_array(time.date, :years, event.interval, until)
    end
  end

  defp get_until(%{"count": count} = event) when not is_nil(count) do
    get_repeat_times(count, event)
  end

  defp get_until(%{"until": until} = _event) when not is_nil(until) do
    until
    |> Ecto.Date.to_iso8601()
    |> Date.from_iso8601!()
  end

  defp get_until(event) do
    # in case of repeat forever, default count = 200
    count = 200
    get_repeat_times(count, event)
  end

  defp get_repeat_times(count, %{"frequency": frequency, "weekday": weekday})
                        when (frequency == "Weekly" or frequency == "Monthly") and not is_nil(weekday) do
    num_of_week_days =
      weekday
      |> String.trim(",")
      |> String.split(",")
      |> Enum.count

    # get number of repeat weeks
    count
    |> Kernel./(num_of_week_days)
    |> Float.ceil()
    |> trunc()
    |> Kernel.-(1)
  end

  defp get_repeat_times(count, _event) do
    count - 1
  end

  defp create_date_array(date, frequency, interval, until, days_of_week) do
    days_of_week
    |> String.trim(",")
    |> String.split(",")
    |> Enum.flat_map(fn(weekday) -> move_start_date(date, frequency, interval, until, weekday) end)
  end

  defp create_date_array(date, frequency, interval \\ 1, until \\ 0) do
    duration =
      if is_integer(until) do
        Keyword.put([], frequency, until)
      else
        until
      end
    step = Keyword.put([], frequency, interval)
    init_date_array = Interval.new(from: date, until: duration, right_open: false)

    init_date_array
    |> Interval.with_step(step)
    |> Enum.map(&Timex.format!(&1, "%Y-%m-%d", :strftime))
  end

  defp move_start_date(date, :weeks, interval, until, weekday) do
    checked_weekday = @weekday[weekday]

    start_date = get_start_date(date, checked_weekday, :weeks)

    create_date_array(start_date, :weeks, interval, until)
  end


  defp move_start_date(date, :months, interval, until, weekday) do
    {week_count, weekday} = String.split_at(weekday, 1)

    n = String.to_integer(week_count)
    checked_weekday = @weekday[weekday]

    date
    |> create_date_array(:months, interval, until)
    |> Enum.map(&Date.from_iso8601!(&1))
    |> Enum.map(&get_start_date(&1, checked_weekday, :months, n))
    |> Enum.filter(&(&1))
  end

  defp get_start_date(date, checked_weekday, frequency, n \\ 1) do
    input_weekday = Date.day_of_week(date)

    updated_date =
      cond do
        checked_weekday > input_weekday ->
          Timex.shift(date, days: checked_weekday - input_weekday + (n - 1) * 7)
        checked_weekday < input_weekday ->
          Timex.shift(date, days: checked_weekday - input_weekday + n * 7)
        true ->
          Timex.shift(date, days: (n - 1) * 7)
      end

    {_, current_month, _} = Date.to_erl(date)
    {_, updated_month, _} = Date.to_erl(updated_date)

    if frequency == :months do
      if current_month == updated_month do
        Date.to_string(updated_date)
      else
        nil
      end
    else
      updated_date
    end
  end

  defp check_date_and_time_overlap(event, instance_date_array, target_date_array, instance, target) do
    overlapped_event =
      if check_current_event_and_update_timeline(event, target_date_array) do
        check_same_date_in_future(event, instance_date_array, target_date_array)
      else
        nil
      end

    if overlapped_event && check_time_overlap(instance, target) do
      overlapped_event
    else
      event
    end
  end

  defp check_current_event_and_update_timeline(event, target_date_array) do
    if is_current_event(target_date_array) do
      true
    else
      event
      |> change(%{is_current: false})
      |> Repo.update()

      false
    end
  end

  # when until date is less than the current time
  defp is_current_event(nil) do
    false
  end

  defp is_current_event(target_date_array) do
    last_date =
      target_date_array
      |> List.last()
      |> Date.from_iso8601!()

    current_date = ecto_to_local_datetime(Ecto.DateTime.utc(), :date)

    Date.compare(last_date, current_date) != :lt
  end

  defp check_same_date_in_future(event, instance_date_array, target_date_array) do
    same_date =
      instance_date_array
      |> Enum.filter(fn(x) -> Enum.any?(target_date_array, fn(y) -> check_same_date_in_future(x, y) end) end)
      |> Enum.filter(fn(x) -> !is_exception_date(x, event.date_exception) end)
      |> List.first

    if same_date do
      Map.merge(event, %{same_date: same_date})
    else
      nil
    end
  end

  defp check_same_date_in_future(date_str, date_str) do
    check_date = Date.from_iso8601!(date_str)
    current_date = get_current_date()

    Date.compare(check_date, current_date) != :lt
  end

  defp check_same_date_in_future(_, _) do
    false
  end

  defp is_exception_date(_, nil) do
    false
  end

  defp is_exception_date(date, date_exception) do
    Enum.any?(date_exception, fn x -> x == date end)
  end

  defp check_time_overlap(instance, target) do
    instance_start = Time.from_iso8601!(instance.start_time)
    instance_end = Time.from_iso8601!(instance.end_time)
    target_start = Time.from_iso8601!(target.start_time)
    target_end = Time.from_iso8601!(target.end_time)

    Time.compare(instance_start, target_end) == :lt && Time.compare(target_start, instance_end) == :lt
  end

  # Change event title when room change name
  def change_all_titles(room, params) do
    RoomPlz.Event
    |> is_current
    |> with_room_and_branch
    |> from_room(room)
    |> Repo.all
    |> Enum.each(&change_event_title(&1, params))
  end

  defp change_event_title(event, params) do
    new_title =
      ~r/\[(.*)\]/
      |> Regex.replace(event.title, "[#{params["name"]}]")

    # Local update
    event
    |> change(%{title: new_title})
    |> Repo.update()

    # Google event update
    g_event =
      event
      |> Map.merge(%{title: new_title})
      |> initiate_event()

    GEvent.update(params["client"], g_event)
  end

  def update_exception_date(%{date_exception: nil, except_date: except_date} = event) do
    # if exception date is equal to date array => deny the action
    if event.date_array == [except_date] do
      change(event, %{status: "rejected"})
    else
      change(event, %{date_exception: [except_date]})
    end
  end

  def update_exception_date(%{date_exception: date_exception, except_date: except_date} = event) do
    if Enum.any?(date_exception, fn date -> date == except_date end) do
      change(event, %{})
    else
      new_exception = Enum.concat(date_exception, [except_date])
      change(event, %{date_exception: new_exception})
    end
  end

  # show creator ID
  def show_creator_id(%{}, user) do
    user.id
  end

  def show_creator_id(%RoomPlz.Event{creator_id: id}, _user) do
    id
  end
end
