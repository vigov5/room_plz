// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"

// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

// Toast set up
toastr.options = {
    closeButton: true,
    preventDuplicates: true,
    closeDuration: 300,
    positionClass: "toast-bottom-right"
};

// Check if calendar page
window.onload = function(){ document.getElementById("loading").style.display = "none" };

let isCalendar = window.location.href.match(/.*calendar/g);
let isCreationEligible = window.location.href.match(/(.*calendar|.*events)/g);
let isCarCalendar = window.location.href.match(/(cars|logs|slot_logs)/g);
if (isCalendar) {
    $("#main").removeClass("container").addClass("container-fluid")
}

// Prevent submit form on enter key
$(document).on("keypress", 'form', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13 && isCreationEligible) {
        e.preventDefault();
        return false;
    }
});

// Navigation
$(".dropdown").hover(
    function () {
        $(this).find("> .dropdown-menu").show()
    }, function () {
        $(this).find(".dropdown-menu").hide()
    }
);

// Home index selection
$("#branch-picker").change(function () {
    $(".branch-selection").attr("href", "/branches/" + $(this).val() + "/calendar");
});

$(".no-branch").click(function () {
    toastr.error("There is no branch available")
});

// Check branch location
let default_branch_id = $("#user-default-branch").val();
let location_branch_id = parseInt(window.location.href.replace(/.*branches\//,""));
let branch_id = location_branch_id || default_branch_id;
if (branch_id) {
    $(".top-nav").css("display", "block");
    $(".top-nav-rooms").attr("href", `/branches/${branch_id}/rooms`).show();
    $(".top-nav-events").attr("href", `/branches/${branch_id}/events`).show();
    $(".top-nav-calendar").attr("href", `/branches/${branch_id}/calendar`);

    $.ajax({
        url: "/api/branches/" + branch_id,
        success: function (branch) {
            $("#top-nav-branch").text(branch.name);
        }
    })
} else {
    $(".top-nav-events, .top-nav-rooms").hide();
}

if (isCarCalendar) {
    $(".top-nav-calendar").attr("href", "/car_calendar");
}

if (location_branch_id) {
    $("#nav-branch").show();
} else {
    $("#nav-branch").hide();
}

// Datatable
require('datatables.net')();
require('datatables.net-bs')();
$('#search-table').DataTable({
    "lengthMenu": [ 10, 25, 50, 100 ],
    "pageLength": 50,
    "order": [ 0, 'desc' ]
});

// User edit
$("#user-edit").click(function () {
   $("input[name='user[email]'], input[name='user[role]']").attr("disabled", false);
});

$("#password-edit").click(function () {
    if ($(".password-field").hasClass("hidden")) {
        $(".password-field").show().removeClass("hidden");
        $(".password").attr("disabled", false);
    } else {
        $(".password-field").hide().addClass("hidden");
        $(".password").attr("disabled", true);
    }
    event.preventDefault();
});