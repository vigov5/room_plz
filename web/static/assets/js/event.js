$(document).ready(function () {
    let room_id = parseInt(window.location.href.replace(/.*room_id=/,""));

    /**
     *** Form Handling
     **/

    // Initial Setup
    let weekdays = {"Daily": "day(s)", "Weekly": "week(s)", "Monthly": "month(s)", "Yearly": "year(s)"};
    let start_value = $(".start").val();
    let end_value = $(".end").val();
    if (start_value == "") {
        $(".date").flatpickr({
            defaultDate: "today"
        });

        generateTimePicker(".start", "08:00");
        generateTimePicker(".end", "14:00");
    } else {
        $(".date").flatpickr({
            defaultDate: moment(start_value).format("YYYY-MM-DD")
        });

        generateTimePicker(".start", moment(start_value).format("HH:mm"));
        generateTimePicker(".end", moment(end_value).format("HH:mm"));
    }

    collaborators = getCollaboratorList();
    invitations = getInvitationList($("#user-id").val());
    var check = function(){
        if(collaborators && invitations){
            appendCollaboratorSuggestions(collaborators);
            appendInvitationSuggestions(invitations);
        }
        else {
            setTimeout(check, 1000); // check again in a second
        }
    };
    check();

    // Default room selector
    if (room_id) {
        $(".room-picker").val(room_id)
    }

    // Define modal
    let recurrance = new jBox('Modal', {
        closeOnEsc: true,
        closeButton: 'title',
        title: "<h4 class='text-center'><strong>Repeat Selection</strong></h4>",
        content: $("#repeat-event"),
        minWidth: 500,
        minHeight: 100
    });

    // Handle click event on modal box
    $("#repeat-checkbox").on("click", function () {
        // After click, check box value has been changed => evaluate
        if ($(this).prop("checked")) {
            recurrance.open();
            $(".start-date, .end-date").flatpickr({
                defaultDate: $(".date").val()
            });
            $("#repeat-checkbox").prop("checked", false);
        } else {
            $(this).prop("checked", false);
            $("#summary-capture").val("").hide();
            $("#repeat-edit").hide();
        }
    });

    $(".repeat-accept").click(function () {
        let $summary = $("#summary-capture");
        $summary.val(
            $("#repeat-summary").text()
        );
        $("#repeat-edit").show();
        $summary.show();

        // set up hidden value
        $("input[name='event[frequency]']").val(
            $("#frequency").val()
        );
        $("input[name='event[interval]']").val(
            $("#interval").val()
        );

        let weekday = "";
        if ($("#frequency").val() == "Weekly") {
            $(".dayOfWeek").each(function () {
                if ($(this).prop("checked")) {
                    weekday += $(this).val().substring(0,2).toUpperCase() + ",";
                }
            })
        }

        // Monthly repeat
        if ($("#frequency").val() == "Monthly") {
            $(".dayOfWeek").each(function () {
                if ($(this).prop("checked")) {
                    weekday += $("#week-order").val() + $(this).val().substring(0,2).toUpperCase() + ",";
                }
            });
        }
        $("input[name='event[weekday]']").val(weekday);

        let end = $("input[type=radio]:checked").val();
        if (end == "count") {
            $("input[name='event[count]']").val(
                $("input[name=quantity]").val()
            );
            $("input[name='event[until]']").val("");
        } else if (end == "until") {
            $("input[name='event[until]']").val(
                $(".end-date").val()
            );
            $("input[name='event[count]']").val("");
        } else {
            $("input[name='event[count]']").val("");
            $("input[name='event[until]']").val("");
        }

        recurrance.close();
        $("#repeat-checkbox").prop("checked", true);
    });

    $(".repeat-cancel").click(function () {
        recurrance.close();
    });

    // Handle click event on form
    $("body").on("click", "#repeat-edit", function () {
        $(".start-date, .end-date").flatpickr({
            defaultDate: $(".date").val()
        });

        let repeatSummary =
            $("#summary-capture").val()
                .replace(/\(s\) (.*)/, "(s)")
                .replace(/[0-9]+/, "<span id='interval-value'>1</span>")
            + "<span id='occurrences'>" + "</span>"
            + "<span id='datePicker'> </span>";
        $("#repeat-summary").html(repeatSummary);
        recurrance.open();
    });

    $(".form-submit").click(function () {
        $("#summary-capture").removeAttr("disabled");
        $("input[name='event[status]']").val("approved");
        $("input[name='event[action]']").val("approved");

        // convert to datetime
        convertToDateTime("start");
        convertToDateTime("end");
    });

    // Define changes
    $(".room-picker").change(function () {
        let title = $("#event_title").val();
        let newRoom = $(".room-picker option:selected").text();
        let newTilte = title.replace(/^\[.*\]\s/, "[" + newRoom + "] ");
        $("#event_title").val(newTilte);
    });

    $("#frequency").change(function () {
        let frequency = weekdays[`${$(this).val()}`];
        $("#repeat-summary").html("Every <span id='interval-value'>" + $("#interval").val() + "</span> " + frequency + "<span id='occurrences'>" + "</span>");
        $("input[value='never']").prop('checked', true);

        if ($(this).val() == "Weekly" || $(this).val() == "Monthly") {
            $(".weekday").show();
            // Re setup
            $(".dayOfWeek").prop("checked", false);

            $("#repeat-summary").append("<span id='datePicker'> </span>");
        } else {
            $(".weekday").hide();
        }

        if ($(this).val() == "Monthly") {
            $(".week-order").show();
        } else {
            $(".week-order").hide();
        }
    });

    $("#interval").change(function () {
        $("#interval-value").html(
            $(this).val()
        );
    });

    $(".dayOfWeek").change(function () {
        $("#datePicker").html("");
        let datePicker = false;
        $(".dayOfWeek").each(function () {
            if ($(this).prop("checked")) {
                if (!datePicker && $("#frequency").val() == "Monthly") {
                    $("#datePicker").append(" on <span id='week-order-span'>" + $("#week-order").find(":selected").text() + "</span>");
                } else if (!datePicker) {
                    $("#datePicker").append(" on");
                }
                datePicker = true;
                let day = $(this).val();
                $("#datePicker").append(" " + day + " ");
            }
        });
    });

    $("#week-order").change(function () {
       $("#week-order-span").text($(this).find(":selected").text());
    });

    $("input[type=radio]").change(function () {
        let endDateType = $(this).val();
        let $occurrences = $("#occurrences");
        let string;
        switch (endDateType) {
            case "count":
                let $numOfOcc = $("input[name=quantity]");
                string = " " + $numOfOcc.val() + " times";
                $occurrences.html(string);

                $numOfOcc.change(function () {
                    if (endDateType == "count") {
                        string = " " + $(this).val() + " times";
                        $("#occurrences").html(string);
                    }
                });
                break;

            case "until":
                let $endDate = $(".end-date");
                string = " until " + $endDate.val();
                $occurrences.html(string);
                $endDate.change(function () {
                    if (endDateType == "until") {
                        string = " until " + $(this).val();
                        $("#occurrences").html(string);
                    }
                });
                break;
            default:
                $occurrences.html("");
        }
    });

    // Supporting function
    function convertToDateTime(time) {
        let $time_field = $("input[name='event[" + time + "]']");
        let datetime = moment(
            $("input[name='event[date]']").val() + " " + $time_field.val()
        ).utc().format();
        $time_field.val(datetime);
    }

    /**
     *** Event Index
     **/

    // Handle on change of selection
    $(".selectpicker").change(function () {
        $(".index-new-event").attr("href", "/branches/" + $(this).val() + "/events/new");
        $(".index-event").attr("href", "/branches/" + $(this).val() + "/events");
    });

    /**
     *** Branch Index
     **/

    // Define modal
    new jBox('Modal', {
        attach: $(".new-event, .new-room"),
        closeOnEsc: true,
        closeButton: "title",
        title: "<h4 class='text-center'><strong>Branch Selection</strong></h4>",
        content: $(".branch-selection"),
        minWidth: 280,
        minHeight: 150,
    });

    $(".new-event").click(function () {
        $(".branch-new-room").hide();
    });

    $(".new-room").click(function () {
        $(".branch-new-event").hide();
    });

    $("#branch-picker").change(function () {
        $(".branch-new-event").attr("href", "/branches/" + $(this).val() + "/events/new");
        $(".branch-new-room").attr("href", "/branches/" + $(this).val() + "/rooms/new");
    });

    /**
     *** Room suggestion  
     **/

    $(".duration").flatpickr({
        enableTime: true,
        noCalendar: true,
        defaultDate: "1:00",
        minuteIncrement: 30,
        time_24hr: true
    });
    
    $(".time-length").change(function () {
        if ($(this).val() == "day") {
            $(".type option[value='duration']").show();
        } else {
            $(".type option[value='duration']").hide();
            $(".type").val("block");
            $(".duration-field").hide();
            $(".start-field, .end-field").show();
        }
    });

    $(".type").change(function () {
        if ($(this).val() == "block") {
            $(".duration-field").hide();
            $(".start-field, .end-field").show();
        } else {
            $(".start-field, .end-field").hide();
            $(".duration-field").show();
        }
    });

    $("#show-suggestion").click(function () {
        event.preventDefault();
        $(".panel-room").show();
        $(".panel-footer").html("");
        $(".panel-footer").text("Checking data in server. Please wait (may last a minute) ...");

        $("#summary-capture").removeAttr("disabled");

        // convert to datetime
        start = $("input[name='event[start]']").val();
        end = $("input[name='event[end]']").val();
        convertToDateTime("start");
        convertToDateTime("end");
        data = setupData();

        $.ajax({
            url: "/api/suggestion",
            method: "GET",
            data: data,
            success: function (resp) {
                $(".panel-footer").html("");
                if (resp.length) {
                    let result_ul = $("<ul></ul>");
                    let result_available = false;
                    resp.forEach(function (result) {
                        if (result.date) {
                            if (result.rooms.length > 0) {
                                result_available = true;
                                daysOfWeek = moment(result.date).day();
                                if (daysOfWeek == 0 || daysOfWeek == 6) {
                                    html = `<strong style="color: red">${moment(result.date).format("YYYY-MM-DD (ddd)")}</strong>`;
                                } else {
                                    html = `<strong>${moment(result.date).format("YYYY-MM-DD (ddd)")}</strong>`;
                                }
                                html += "<ul style='margin-top: 0'>";
                                result.rooms.forEach(function (room) {
                                    html += `<li><strong>${room.name}</strong> - Capacity: ${room.capacity}</li>`
                                });
                                html += "</ul> <hr>";

                                result_ul.append(html);
                            }
                        } else {
                            result_available = true;
                            id = "available-time-" + result.id;
                            result_ul.append(
                                `<li>
                                    <strong>${result.name}</strong> - Capacity: ${result.capacity}
                                    <ul id= ${id} style="margin-top: 5px"></ul>
                                </li>`
                            );

                            if (data.duration) {
                                result.times.forEach(function (time) {
                                    result_ul.find("#" + id).append(`<li>${time.start_time} - ${time.end_time}</li>`)
                                });
                                result_ul.append("<hr>");
                            }
                        }

                    });
                    if (!result_available) {
                        $(".panel-footer").text("There is no room available")
                    }

                    $(".panel-footer").append(result_ul);
                } else {
                    $(".panel-footer").text("There is no room available at the requested time")
                }
            }
        });

        // reset field
        $("input[name='event[start]']").val(start);
        $("input[name='event[end]']").val(end);
        $("#summary-capture").attr("disabled", "true");
    });


    function setupData() {
        let data = {
            "branch_id": $("select[name='event[branch]']").val(),
            "time_length": $("select[name='event[time_length]']").val()
        };
        capacity = $("input[name='event[capacity]']").val();
        if (capacity) {
            data.capacity = capacity;
        }

        if ($(".type").val() == "block") {
            data.start = $("input[name='event[start]']").val();
            data.end = $("input[name='event[end]']").val();
        } else {
            list_of_info = $("input[name='event[duration]']").val().split(":");
            list_of_info.push($("input[name='event[date]']").val());

            data.duration = list_of_info;
        }

        if ($("#repeat-checkbox").prop("checked")) {
            data.frequence = $("#frequency option:selected").val();
            data.interval = $("#interval").val();
            if (data.frequence == "Weekly") {
                data.weekday = "";
                $(".dayOfWeek").each(function () {
                    if ($(this).prop("checked")) {
                        data.weekday += $(this).val().substring(0,2) + ",";
                    }
                });
            }

            let $endDate = $("input[type=radio]:checked").val();
            if ($endDate == "count") {
                data.count = $("input[name=quantity]").val();
            } else if ($endDate == "until") {
                data.until = $(".end-date").val();
            }
        }

        // Unnecessary information, just put a number to pass validate_required in changeset
        data.creator_id = 1;
        data.title = "random string";
        data.status = "approved";

        return data;
    }
});