let a, v, e, clr, filtered;
roomIds = [];
rooms = [];
jQuery(document).ready(function () {
    // Get date params if exists
    if (window.location.href.indexOf("date") == -1) {
        date = moment(new Date()).format("YYYY-MM-DD");
    } else {
        date = window.location.href.replace(/.*date=/,"")
    }

    clr = $('#calendar');
    userId = clr.data("user-id");
    branchId = clr.data("branch-id");
    taskbarLeft = $("#taskbar").offset().left;
    taskbarWidth = $("#taskbar").outerWidth();
    expiredMonth = 6;
    expireVal = -1;
    overlapVal = -2;
    myEventVal = -3;

    if (userId) {
        collaborators = getCollaboratorList();
        invitations = getInvitationList(userId);
    }
    // Display authorize button
    if (!isSuperAdmin()) {
        $(".authentication").addClass("invisible");
    }

    new jBox('Modal', {
        title: "<h2>Google Authorization</h2>",
        content: $("#gg-auth"),
        attach: $(".gg-auth"),
        width: 350,
        closeOnClick: false,
        closeButton: "title"
    });

    // Set calendar flatpickr
    mini = $("#minicalendar").flatpickr({
        defaultDate: date,
        onChange: function (selectedDates, dateStr) {
            changeUrl(dateStr);

            clr.fullCalendar('gotoDate', new Date(dateStr));
        }
    });

    $("#synchronize-datetime").flatpickr({
        enableTime: true,
        defaultDate: "today",
        time_24h: true,
        defaultHour: 0,
        defaultMinute: 0
    });

    // Get rooms from server
    $.ajax({
        url: '/api/rooms',
        type: 'GET',
        data: {
            branch_id: branchId
        },
        success: function (data) {
            rooms = data;
            roomNames = data.map(function (r) {
                return r.name.toLowerCase()
            });

            titleWidth = taskbarWidth / rooms.length;
            hightlightRoomTitle();
        }
    });

    setTimeout(function () {
        // create the row title
        for (i = 0; i < rooms.length; i++) {
            if (rooms[i].overlap_acceptance) {
                row = `*${rooms[i].name}`
            } else {
                row = `${rooms[i].name}`
            }

            if (rooms[i].name != "Other") {
                row = row + ` (${rooms[i].capacity})`;
            }

            $('#row-content').append(`<td class="room-title" data-row-id=${i}>${row}</td>`);
        }
        $('#row-content').append('<td></td>');
    }, 500);

    // Set up full calendar
    clr.fullCalendar({
        // put your options and callbacks here
        customButtons: {
            synchronize: {
                icon: 'synchronize',
                click: function() {
                    $("#synchronize-button").unbind().click(function () {
                        let startTimeSync = moment(
                            $("#synchronize-datetime").val()
                        ).utc().format();

                        synchronizeEvents(startTimeSync);
                        syncJbox.close();
                    })
                },
            },
            "new-event": {
                icon: 'new-event'
            }
        },
        header: {
            right: 'synchronize new-event today prev,next',
            center: 'title',
            left: false
        },
        defaultDate: date,
        nowIndicator: true,
        defaultView: 'agendaDay',
        minTime: '06:00:00',
        allDaySlot: true,
        allDayText: 'Room',
        selectable: isAdmin(),
        selectHelper: true,

        // get events from GG API
        googleCalendarApiKey: googleCalendar.API_KEY,
        eventSources: {
            googleCalendarId: clr.data("calendar-id")
        },

        eventAfterRender: function (event, element) {
            // CSS for events
            $(".fc-event-container a.fc-event").click(function (e) {
                e.preventDefault();
                $(this).css("color", "white")
            });
            $(".fc-event-container a.fc-event").focus(function () {
                $(this).css("text-decoration", "none");
            });

            // Get event ID
            if ((typeof event.id) == "string") {
                event.id = parseInt(event.id.replace(appendToCalendarId, ""));
            }

            // description format = creator/id/(repeat)/([collaborators])
            if (isJsonString(event.description)) {
                description = JSON.parse(event.description)
            } else {
                description = {
                    team_id: -1,
                    creator_id: -1,
                    creator: "Online Calendar",
                    repeat: "---"
                }
            }
            let owner = isOwner(userId, description.creator_id);
            let collaborator = isCollaborator(userId, description.collaborators);

            // Enable editable
            event.editable = clr.data('editable') && (isAdmin() || owner || collaborator);

            // Get room name
            let regex = /^\[(.*?)].*|(.*)\:.*/;
            let match = regex.exec(event.title);
            if (match) {
                roomName = match[1] || match[2];
            } else {
                roomName = "Other"
            }

            // Change color & room name to "Other" if not found
            let pos = roomNames.indexOf(roomName.toLowerCase());
            if (pos == -1) {
                pos = roomNames.indexOf("other");
            }

            // positioning each event
            positionElement(pos);
            event.position = pos;

            // Add config to element
            if (checkOverlap(event, element)) {
                isOverlap = true;
            } else {
                isOverlap = false;
            }

            if (moment(description.start_date).add(expiredMonth, 'months').isAfter(moment())) {
                isExpired = false;
            } else {
                isExpired = true;
            }

            if (owner || collaborator) {
                isMyEvent = true;
            } else {
                isMyEvent = false;
            }

            element.addClass("calendar-events");
            element.attr({
                "data-overlap": isOverlap,
                "data-my-event": isMyEvent,
                "data-expire": isExpired,
                "data-team-id": description.team_id,
                "data-team-color": description.color,
                "data-room-color": rooms[pos].color
            });

            // Remove repeat in approved event
            $("#event-popup").find("#event-repeat").html("");

            // Change event css
            if (event.end < Date.now()) {
                element.css('opacity', 0.5);
            }

            function positionElement(pos) {
                let width = 100 / rooms.length;
                l = pos * width;
                element.css({
                    'margin-right': 0,
                    'left': l + '%',
                    'width': width + '%',
                });
            }

            // Create box when hover
            if (!event.tip) {
                event.tip = new jBox('Tooltip', {
                    title: event.title,
                    content: initDialog(event, description),
                    attach: $(element),
                    closeOnMouseleave: true,
                    adjustPosition: 'flip',
                    adjustPosition: {top: 50, right: 5, bottom: 20, left: 5},
                    repositionOnOpen: true
                });
            }
        },

        eventAfterAllRender: function () {
            mini.setDate(
                clr.fullCalendar('getDate').format("YYYY-MM-DD")
            );
            check_team_and_change_color($("#team-picker").val());
            checkWarning("overlap");
            checkWarning("expire");

            function checkWarning(attr) {
                if ($(`.calendar-events[data-${attr}=true]`).length == 0) {
                    $(`#warning-${attr}`).hide();
                } else {
                    $(`#warning-${attr}`).show();
                }
            }
        },

        select: function (start, end) {
            let titlePosition = Math.floor((mouseDownX - taskbarLeft) / titleWidth);
            let titleId = rooms[titlePosition].id;

            let newEvent = new jBox('Modal', {
                closeOnEsc: true,
                closeButton: 'title',
                title: "<h4>Event</h4>",
                content: initNewEvent(start, end, titleId),
                minWidth: 350,
            });

            newEvent.open();
            appendCollaboratorSuggestions(collaborators);
            appendInvitationSuggestions(invitations);

            // Force Create Event
            $(".form-submit, .force-submit").click(function () {
                event.preventDefault();
                newEvent.close();
                let is_forced = $(this).hasClass("force-submit");
                if (!is_forced || (is_forced && confirm("Override overlapped events?"))) {
                    submitEvent($(this), start, end, is_forced)
                }
            });

            clr.fullCalendar('unselect');
        },

        eventDrop: function (event, delta, revertFunc) {
            if (confirm("Are you sure about this change?")) {
                updateEvent(event, revertFunc);
            } else {
                revertFunc();
            }
        },

        eventResize: function (event, delta, revertFunc) {
            if (confirm("Are you sure about this change?")) {
                updateEvent(event, revertFunc);
            } else {
                revertFunc();
            }
        }
    });

    /*
    ** Custom JQuery
    */

    function submitEvent($this, start, end, is_forced) {
        // Get invitation list
        let invitation = getTags("#invitation-div");
        let collaborators = getTags("#collaborator-div");
        let parent = $this.parent();

        adjustedTime = moment.duration(timezone.adjustedTime.replace("+", "").concat(":00"));
        data = {
            "event[form_type]": "ajax",
            "event[creator_id]": parent.find("input[name='event[creator_id]']").val(),
            "event[room_id]": parent.find("#room-picker").val(),
            "event[title]": parent.find("input[name='event[title]']").val(),
            "event[start]": start.subtract(adjustedTime).format(),
            "event[end]": end.subtract(adjustedTime).format(),
            "event[team_id]": parent.find("select[name='event[team_id]']").val(),
            "event[color]": parent.find("input[name='event[color]']").val(),
            "event[invitation]": invitation,
            "event[collaborators]": collaborators,
            "event[status]": "approved",
            "event[action]": "approved"
        };

        if (is_forced) {
            data["event[force]"] = true;
        }

        $.ajax({
            url: `/branches/${branchId}/events`,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
            },
            method: "POST",
            data: data,
            success: function (resp) {
                showResp(resp);
            }
        })

            .fail(function (resp) {
                console.log(resp.responseJSON);
            })
    }

    setInterval(function () {
        clr.fullCalendar("refetchEvents");
    }, 5 * 60 * 1000);

    function initDialog(event, description) {
        time_txt = event.start.format("dddd, MMMM Do, h:mm a") + " - " + event.end.format("h:mm a");
        tpl = $("#event-popup");
        tpl.find("#event-when").html(time_txt);

        tpl.find("#event-creator").html(description.creator);

        // Check if the third element exists and in not collaborators
        $("#event-popup").find("#event-repeat").html(`Repeat: ${description.repeat} <br>`);

        if (description.team_id == -1) {
            // Online calendar events
            tpl.find(".details").attr("href", event.url).show();
            tpl.find(".edit, .reject").hide();
        } else {
            tpl.find(".details").attr("href", "/branches/" + branchId + "/events/" + event.id).show();
            if (isAdmin() || isOwner(userId, description.creator_id) || isCollaborator(userId, description.collaborators)) {
                tpl.find(".edit").attr("href", "/branches/" + branchId + "/events/" + event.id + "/edit").show();
                tpl.find(".delete, .reject").attr("data-event-id", event.id).show();
                tpl.find(".reject").attr("data-start-time", moment(event.start).utc().format());
            } else {
                tpl.find(".edit, .reject").hide()
            }
        }

        return tpl.html();
    }

    function initNewEvent(start, end, titleId) {
        time_txt = start.format("dddd, MMMM Do, h:mm a") + " - " + end.format("h:mm a");
        tpl = $("#new-event");
        tpl.find("input[name='event[start]']").val(start.utc().format());
        tpl.find("input[name='event[end]']").val(end.utc().format());
        tpl.find("#datetime").text(time_txt);
        tpl.find("#room-picker option").attr("selected", false);
        tpl.find(`#room-picker option[value=${titleId}]`).attr("selected", true);

        invitationDiv = "<label class=\"control-label\">Invitations (**Press <ins>Enter</ins> after each input)</label>\n" +
                        "<span class='badge' data-toggle='tooltip' data-placement='top' title='Go to profile to change invitation suggestion'>\n" +
                        `<a href='/users/${userId}/edit' id='edit-user'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></a></span>:` +
                        "<input type=\"text\" class=\"form-control\" id=\"invitations\" data-role=\"tagsinput\" />";
        collaboratorDiv = "<label class=\"control-label\">Collaborators (**Press <ins>Enter</ins> after each input)</label>\n" +
                          "<input type=\"text\" class=\"form-control\" id=\"collaborators\" data-role=\"tagsinput\" />";
        tpl.find("#invitation-div, #collaborator-div").html("");
        tpl.find("#invitation-div").append(invitationDiv);
        tpl.find("#collaborator-div").append(collaboratorDiv);

        return tpl.html();
    }

    // Change URL
    function changeUrl(dateStr) {
        if (window.location.href.indexOf("date") == -1) {
            new_href = `${window.location.href}?date=${dateStr}`
        } else {
            new_href = window.location.href.replace(/date=.*/, `date=${dateStr}`)
        }
        window.history.pushState(null, null, new_href);
    }

    // custom calendar button
    $(".fc-icon-synchronize, .fc-icon-new-event").remove();
    $(".fc-synchronize-button").append("<i class='fa fa-refresh' aria-hidden='true'></i> Sync");
    $(".fc-synchronize-button").addClass("btn btn-success");
    $(".fc-new-event-button").append("<i class='fa fa-plus' aria-hidden='true'></i> Event");
    $(".fc-new-event-button").addClass("btn btn-primary");

    if (isAdmin()) {
        $(".fc-synchronize-button").css("visibility", "visible")
    } else {
        $(".fc-synchronize-button").css("visibility", "hidden")
    }

    if (userId) {
        $(".fc-new-event-button").css("visibility", "visible")
    } else {
        $(".fc-new-event-button").css("visibility", "hidden")
    }

    $(".fc-new-event-button").click(function () {
        window.location.replace(`/branches/${branchId}/events/new`);
    });

    // jBox synchronize
    let syncJbox = new jBox('Modal', {
        title: "<h2>Synchronize event</h2>",
        content: $("#synchronize-div"),
        attach: $(".fc-synchronize-button"),
        width: 300,
        closeOnClick: false,
        closeButton: "title"
    });

    /*
    ** Calendar title
     */

    // Show room title selection on mouse moving
    function hightlightRoomTitle() {
        $(document).mousemove(function(event) {
            let currentX = event.pageX;
            let currentY = event.pageY;
            let taskbarTop = $("#taskbar").offset().top;
            let taskbarHeight = $("#taskbar").outerHeight();

            if (currentX > taskbarLeft && currentX < taskbarLeft + taskbarWidth &&
                currentY > taskbarTop + taskbarHeight) {
                let titlePosition = Math.floor((currentX - taskbarLeft) / titleWidth);
                $("td.room-title").css("background", "#fdf8e3");
                $(`td[data-row-id=${titlePosition}]`).css("background", "yellow");
            } else {
                $("td.room-title").css("background", "#fdf8e3");
            }
        });
    }

    // Get room title on click
    $(document).mousedown(function(event) {
        mouseDownX = event.pageX;
    });

    // Float calendar title
    $(".fc-day-header").css("height", "26px");

    let start_pos = clr.offset().top -5;
    relativeAdjusted = 83;
    $('#room-title').css("top", start_pos + relativeAdjusted);

    $(window).scroll(function () {
        let pos = clr.offset().top,
            scroll = $(window).scrollTop();
        if (scroll >= pos + relativeAdjusted) {
            $('#room-title').css("top", "0");
        }
        else {
            $('#room-title').css("top", pos - scroll + relativeAdjusted - 9);
        }
    });

    // Change Calendar URL
    $(".fc-next-button, .fc-prev-button, .fc-today-button").click(function () {
        let newDate = clr.fullCalendar('getDate').format("YYYY-MM-DD");
        changeUrl(newDate);
    });

    // Navigation
    $(".dropdown").hover(
        function () {
            $(this).find("> .dropdown-menu").show()
        }, function () {
            $(this).find(".dropdown-menu").hide()
        }
    );

    // Team Calendar
    $("#team-picker").change(function () {
        check_team_and_change_color($(this).val())
    });

    function check_team_and_change_color(displayId) {
        $(".calendar-events").each(function () {
            $this = $(this);
            if (displayId == 0) {
                let color = $this.attr("data-room-color");
                $this.css({"background": color, "border-color": color});
            } else if (displayId == expireVal) {
                changeColor($this, "expire", "red", "black");
            } else if (displayId == overlapVal) {
                changeColor($this, "overlap", "red", "black");
            } else if (displayId == myEventVal) {
                changeColor($this, "my-event", "blue", "black");
            } else {
                if (displayId == $this.attr("data-team-id") && $this.attr("data-team-color")) {
                    changeColor($this, $this.attr("data-team-color"))
                } else if (displayId == $this.attr("data-team-id")) {
                    changeColor($this, "blue")
                } else {
                    changeColor($this, "black")
                }
            }
        });
    }

    function changeColor($this, attr, rightColor, falseColor) {
        if ($this.attr(`data-${attr}`) == "true") {
            $this.css({"background": rightColor, "border-color": rightColor});
        } else {
            $this.css({"background": falseColor, "border-color": falseColor});
        }
    }

    // Add overlap attribute
    function checkOverlap(event, element) {
        let start = new Date(event.start);
        let end = new Date(event.end);

        let overlap = $('#calendar').fullCalendar('clientEvents', function (ev) {
            if (ev == event)
                return false;
            let estart = new Date(ev.start);
            let eend = new Date(ev.end);

            return (estart < end && eend > start && ev.position == event.position
                    && !rooms[event.position].overlap_acceptance);
        });

        return overlap.length;
    }

    // Overlap/Expire Display
    $("#warning-overlap, #warning-expire").click(function () {
        let val = $(this).data("val");
        if (val == overlapVal) {
            toastr.success("Showing overlapped events");
        } else if (val == expireVal) {
            toastr.success("Showing outdated events");
        }
        $("#team-picker").val(val);
        check_team_and_change_color(val);
    });

    /**
     * CRUD local events
     */

    // Handle Approve/ Reject event
    $("body").on("click", ".reject", function () {
        event.preventDefault();
        let event_id = $(this).data("event-id");
        let exceptTime = $(this).attr("data-start-time");

        // jBox reject
        rejectJbox = new jBox('Modal', {
            title: "<h2>Reject event</h2>",
            content: initRejectBox(event_id, exceptTime),
            width: 300,
            closeOnClick: false,
            closeButton: "title"
        });

        rejectJbox.open();
    });

    function initRejectBox(event_id, exceptTime) {
        content = $("#reject-div");
        content.find("#reject-single, #reject-all").attr("data-event-id", event_id);
        content.find("#reject-single").attr("data-except-time", exceptTime);

        return content;
    }

    $("body").on("click", "#reject-all", function () {
        let eventId = $(this).attr("data-event-id");
        rejectJbox.close();

        $.ajax({
            url: `/branches/${branchId}/events/${eventId}`,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
            },
            type: "PUT",
            data: {
                "event[form_type]": "ajax",
                "event[status]": "rejected"
            },
            success: function (resp) {
                showResp(resp)
            }
        });
    });

    $("body").on("click", "#reject-single", function () {
        let eventId = $(this).attr("data-event-id");
        let exceptTime = $(this).attr("data-except-time");
        rejectJbox.close();

        $.ajax({
            url: `/branches/${branchId}/events/${eventId}`,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
            },
            type: "PUT",
            data: {
                "event[form_type]": "ajax",
                "event[except-time]": exceptTime
            },
            success: function (resp) {
                showResp(resp)
            }
        });
    });

    // Update event
    function updateEvent(event, revertFunc) {
        status = event.status != undefined ? event.status : "approved";
        // Update local event
        $.ajax({
            url: `/branches/${branchId}/events/${event.id}`,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
            },
            method: "PUT",
            data: {
                "event[form_type]": "ajax",
                "event[status]": status,
                "event[start_time]": event.start.format("HH:mm"),
                "event[end_time]": event.end.format("HH:mm")
            },
            success: function (resp) {
                if (resp.errors || resp.error) {
                    revertFunc();
                }
                showResp(resp)
            }
        })
    }

    /*
    ** Synchronization
     */
    function synchronizeEvents(startTimeSync) {
        isSynchronized = true;
        // Show all local events
        $.ajax({
            url: "/api/events/synchronize",
            method: "POST",
            data: {
                "event[form_type]": "api",
                "branch_id": branchId,
                "date_time": startTimeSync,
            },
            success: function (resp) {
                showResp(resp)
            },
            error: function () {
                toastr.error("Unable to synchronize. Please retry! Otherwise, contact admin")
            }
        });
    }

    /*
    ** Import events
     */
    importJbox = new jBox('Modal', {
        title: "<h2>Import Events</h2>",
        content: $("#get-calendar-id"),
        attach: $(".import-events"),
        width: 350,
        closeOnClick: false,
        closeButton: "title"
    });

    $(".import").click(function () {
        importJbox.close();
        $(".import-events").hide();
        importInterval = setInterval(function () {
            toastr.success("Importing events are in progress. Please wait ...")
        }, 8000);

        $.ajax({
            url: "/api/import_events",
            method: "POST",
            data: {
                "event[form_type]": "api",
                "source_calendar_id": $("#calendar-id").val(),
                "branch_id": branchId
            },
            success: function (resp) {
                clearInterval(importInterval);

                if (resp.success) {
                    $(this).hide();
                }
                showResp(resp)
            },
            error: function () {
                clearInterval(importInterval);
                toastr.error("Unable to import. Please contact admin")
            }
        })
    });

    // Disable event creation for members
    if (!isAdmin()) {
        $("#my-events, .fc-new-event-button").hide();
    }

    /*
    ** Supporting function
     */

    // Show response of ajax call
    function showResp(resp) {
        if (resp.errors) {
            if (resp.errors.overlap) {
                toastr.error(resp.errors.overlap[0]);
            } else {
                toastr.error("Title " + resp.errors.title[0]);
            }
        } else if (resp.error) {
            if (resp.error.indexOf("403") != -1) {
                resp.error = `${resp.error} - Need to ask admin to share calendar with this email, then synchronize`
            }
            toastr.error(resp.error)
        } else if (resp.success) {
            toastr.success(resp.success);
            clr.fullCalendar("refetchEvents");
        }
    }

    function isAdmin() {
        return clr.data("user-role") == "admin" || clr.data("user-role") == "super-admin";
    }

    function isSuperAdmin() {
        return clr.data("user-role") == "super-admin";
    }

    function isOwner(userId, creatorId) {
        return userId == creatorId;
    }

    function isCollaborator(userId, collaborators) {
        if ($.inArray(userId, collaborators) == -1) {
            return false;
        } else {
            return true;
        }
    }

    function isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
});
