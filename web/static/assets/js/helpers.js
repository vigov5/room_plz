function getCollaboratorList() {
    $.ajax({
        url: '/api/users',
        success: function (list_of_collaborator_emails) {
            collaborators = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('email'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: list_of_collaborator_emails
            });

            return collaborators;
        }
    });
}

function getInvitationList(id) {
    $.ajax({
        url: `/api/users/${id}`,
        success: function (list_of_invitation_emails) {
            invitations = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('email'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: list_of_invitation_emails
            });

            return invitations;
        }
    });
}

function appendCollaboratorSuggestions(collaborators) {
    // Conflict in JS, required 2 ids, add random #test to make the file work
    $('#collaborators, #test').tagsinput(
        tagsinputSetup(collaborators)
    );
}

function appendInvitationSuggestions(invitations) {
    $('#invitations, #test').tagsinput(
        tagsinputSetup(invitations)
    );
}

function tagsinputSetup(source) {
    config = {
        confirmKeys: [13, 32, 44],
        typeaheadjs: {
            name: 'emails',
            displayKey: 'email',
            valueKey: 'email',
            source: source
        }
    };
    return config
}


function getTags(element, parent) {
    let tags = "";

    if (parent) {
        parent.find(`${element} > .bootstrap-tagsinput > span.tag`).each(function () {
            tags += $(this).text() + ","
        });
    } else {
        $(`${element} > .bootstrap-tagsinput > span.tag`).each(function () {
            tags += $(this).text() + ","
        });
    }

    tags = tags.substring(0, tags.length - 1);

    return tags;
}

function generateTimePicker(element, time) {
    $(`${element}`).flatpickr({
        enableTime: true,
        noCalendar: true,
        defaultDate: time,
        minuteIncrement: 30,
        time_24hr: true
    });
}