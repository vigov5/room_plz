let a, v, e, clr, filtered;
cars = [];
jQuery(document).ready(function () {
    // Get date params if exists
    if (window.location.href.indexOf("date") == -1) {
        date = moment(new Date()).format("YYYY-MM-DD");
    } else {
        date = window.location.href.replace(/.*date=/,"")
    }

    clr = $('#car-calendar');
    userId = clr.data("user-id");
    taskbarLeft = $("#taskbar").offset().left;
    taskbarWidth = $("#taskbar").outerWidth();

    invitations = getInvitationList(userId);

    // Set calendar flatpickr
    mini = $("#minicalendar").flatpickr({
        defaultDate: date,
        onChange: function (selectedDates, dateStr) {
            changeUrl(dateStr);

            clr.fullCalendar('gotoDate', new Date(dateStr));
        }
    });

    // Get cars from server
    $.ajax({
        url: '/api/cars',
        type: 'GET',
        success: function (data) {
            cars = data;
            carNames = data.map(function (r) {
                return r.name
            });

            titleWidth = taskbarWidth / cars.length;
            hightlightCarTitle();
        }
    });

    setTimeout(function () {
        // create the row title
        for (i = 0; i < cars.length; i++) {
            $('#row-content').append(`<td class="car-title" data-row-id=${i}>${cars[i].name} (${cars[i].capacity})</td>`);
        }
        $('#row-content').append('<td></td>');
    }, 500);


    clr.fullCalendar({
        // put your options and callbacks here
        customButtons: {
            cars: {
                text: 'Cars',
                click: function () {
                    window.location = '/cars'
                }
            },
            slots: {
                text: 'Slots',
                click: function () {
                    window.location = '/slots'
                }
            }
        },
        header: {
            right: 'cars, slots, month,agendaDay, today prev,next',
            center: 'title',
            left: false
        },
        defaultDate: date,
        nowIndicator: true,
        defaultView: 'month',
        minTime: '00:00:00',
        allDaySlot: true,
        allDayText: 'Room',
        editable: userId,
        selectable: userId,
        selectHelper: true,
        slotLabelFormat: 'H(:mm)',
        timeFormat: 'H(:mm)',
        slotDuration: '00:30:00',

        // get events from API
        eventSources: {
            url: '/api/slots',
            error: function () {
                toastr.error("Server Error !");
            }
        },

        eventAfterRender: function (event, element, view) {
            // CSS for events
            $(".fc-event-container a.fc-event").click(function (e) {
                e.preventDefault();
                $(this).css("color", "white")
            });
            $(".fc-event-container a.fc-event").focus(function () {
                $(this).css("text-decoration", "none");
            });

            // Get slot name
            let regex = /^\[(.*?)]\s(.*)/;
            let match = regex.exec(event.title);
            let carName = match[1];
            let pos = carNames.indexOf(carName);

            element.addClass("calendar-slots");
            element.attr("data-position", cars[pos].id);
            element.css("background", cars[pos].color);
            element.css("border-color", cars[pos].color);
            // positioning each event
            if (view.name == 'agendaDay') {
                positionElement(pos);
            } else {
                $(".fc-row .fc-content-skeleton tbody td, .fc-row .fc-helper-skeleton tbody td").css("padding", "0 0 10px 0");
                $("#car-title .fc-row .fc-content-skeleton tbody td, .fc-row .fc-helper-skeleton tbody td").css("padding", "20px 0 16px 0");
                $(".calendar-slots").css("padding", "8px");
                txt = event.start.format("H:mm") + " - " + event.end.format("H:mm");
                element.find("span").first().html(txt)
            }

            // Remove repeat in approved event
            $("#event-popup").find("#event-repeat").html("");

            // Change event css
            if (event.end < Date.now()) {
                element.css('opacity', 0.5);
            }

            function positionElement(pos) {
                let width = 100 / cars.length;
                l = pos * width;
                element.css({
                    'margin-right': 0,
                    'left': l + '%',
                    'width': width + '%',
                });
            }

            // Create box when hover
            if (!event.tip) {
                event.tip = new jBox('Tooltip', {
                    title: event.title,
                    content: initDialog(event),
                    attach: $(element),
                    closeOnMouseleave: true,
                    adjustPosition: 'flip',
                    adjustPosition: {top: 50, right: 5, bottom: 20, left: 5},
                    repositionOnOpen: true
                });
            }
        },

        eventAfterAllRender: function () {
            mini.setDate(
                clr.fullCalendar('getDate').format("YYYY-MM-DD")
            );
        },

        select: function (start, end) {
            let view = clr.fullCalendar("getView");
            if (view.name == "month") {
                end = start;
            }
            let titlePosition = Math.floor((mouseDownX - taskbarLeft) / titleWidth);
            let titleId = cars[titlePosition].id;

            let newEvent = new jBox('Modal', {
                closeOnEsc: true,
                closeButton: 'title',
                title: "<h4>Car</h4>",
                content: initEvent(start, end, titleId, ""),
                minWidth: 450,
            });

            newEvent.open();
            appendInvitationSuggestions(invitations);
            editTime(start, end);

            $(".form-edit").addClass("form-submit").removeClass("form-edit");

            // Create Event
            $(".form-submit").click(function () {
                parent = $(this).parent();
                newEvent.close();
                event.preventDefault();

                data = getData(parent);
                data["slot[action]"] = "approved";

                $.ajax({
                    url: `/slots/`,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
                    },
                    method: "POST",
                    data: data,
                    success: function (resp) {
                        showResp(resp);
                    }
                })
                .fail(function (resp) {
                    console.log(resp.responseJSON);
                })
            });

            clr.fullCalendar('unselect');
        },

        eventDrop: function (event, delta, revertFunc) {
            if (confirm("Are you sure about this change?")) {
                updateEvent(event, revertFunc);
            } else {
                revertFunc();
            }
        },

        eventResize: function (event, delta, revertFunc) {
            if (confirm("Are you sure about this change?")) {
                updateEvent(event, revertFunc);
            } else {
                revertFunc();
            }
        },

        viewRender: function (view) {
            if (view.name == 'month') {
                $("#car-title").hide();
                $(".fc-day-grid .fc-content-skeleton td:last-child").removeClass("last-child");
            } else {
                $("#car-title").show();
                $(".fc-day-grid .fc-content-skeleton td:last-child").addClass("last-child");
            }
        }
    });

    /*
    ** Custom JQuery
    */
    $("body").on("change", "#car-picker", function () {
        parent = $(this).parent().parent();
        let title = parent.find("#car-name").val();
        let newRoom = parent.find("#car-picker option:selected").text();
        let newTilte = title.replace(/^\[.*\]\s/, "[" + newRoom + "] ");
        parent.find("#car-name").val(newTilte);
    });

    setInterval(function () {
        clr.fullCalendar("refetchEvents");
    }, 5 * 60 * 1000);

    function initDialog(slot) {
        time_txt = slot.start.format("dddd (DD/MM): H:mm") + " - " + slot.end.format("H:mm");
        tpl = $("#slot-popup");
        tpl.find("#slot-when").html(time_txt);
        tpl.find("#slot-driver").html(slot.driver);
        tpl.find("#slot-utilizer").html(slot.utilizer);
        tpl.find("#slot-creator").html(slot.creator);
        tpl.find("#slot-description").html(slot.description);

        tpl.find(".details").attr("href", "/slots/" + slot.id).show();
        if (isAdmin() || isOwner(userId, slot)) {
            tpl.find(".delete, .reject, .edit").attr("data-slot-id", slot.id);
        } else {
            tpl.find(".edit, .reject").hide()
        }

        return tpl.html();
    }

    function initEvent(start, end, titleId, invitation) {
        time_txt = start.utc().format("dddd (DD/MM)");
        tpl = $("#new-slot");
        tpl.find("#slot_start").val(start.utc().format());
        tpl.find("#slot_end").val(end.utc().format());
        tpl.find("#datetime").text(time_txt);
        tpl.find("#car-picker option").attr("selected", false);
        tpl.find(`#car-picker option[value=${titleId}]`).attr("selected", true);

        tpl.find("#invitation-div").html("");
        invitationDiv = "<label class='control-label'>Invitations  (**Press <ins>Enter</ins> after each input)</label>" +
            "<span class='badge' data-toggle='tooltip' data-placement='top' title='Go to profile to change invitation suggestion'>\n" +
            `<a href='/users/${userId}/edit' id='edit-user'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></a>\n` +
            "</span>:";
        if (invitation) {
            invitationDiv += "<input type='text' class='form-control' id='invitations' data-role='tagsinput' value="+ invitation +">";
        } else {
            invitationDiv += "<input type='text' class='form-control' id='invitations' data-role='tagsinput' />";
        }
        tpl.find("#invitation-div").append(invitationDiv);

        return tpl.html();
    }
    // Change URL
    function changeUrl(dateStr) {
        if (window.location.href.indexOf("date") == -1) {
            new_href = `${window.location.href}?date=${dateStr}`
        } else {
            new_href = window.location.href.replace(/date=.*/, `date=${dateStr}`)
        }
        window.history.pushState(null, null, new_href);
    }

    // custom Car button
    $(".fc-cars-button").addClass("btn btn-success");
    $(".fc-slots-button").addClass("btn btn-primary");


    if (userId) {
        $(".fc-new-event-button").css("visibility", "visible")
    } else {
        $(".fc-new-event-button").css("visibility", "hidden")
    }

    $(".fc-new-event-button").click(function () {
        window.location.replace(`/branches/${branchId}/events/new`);
    });

    // Edit time on jBox
    function editTime(start, end) {
        $(".start").val(start.utc().format("H:mm"));
        $(".end").val(end.utc().format("H:mm"));

        $(".start, .end").change(function () {
            let val = $(this).val();
            if (val.match(/^[0-9]{2}:[0-9]{2}$/g)) {
                $(".time-format-error").hide();
                if ($(this).hasClass("start")) {
                    let currentVal = $("#slot_start").val();
                    let newVal = currentVal.replace(/^(.{11}).{5}(.{4})$/, "$1" + val + "$2");
                    $("#slot_start").val(newVal);
                } else {
                    let currentVal = $("#slot_end").val();
                    let newVal = currentVal.replace(/^(.{11}).{5}(.{4})$/, "$1" + val + "$2");
                    $("#slot_end").val(newVal);
                }
            } else {
                $(".time-format-error").show()
            }
        })
    }

    /*
    ** Calendar title
     */

    // Show car title selection on mouse moving
    function hightlightCarTitle() {
        $(document).mousemove(function(event) {
            let currentX = event.pageX;
            let currentY = event.pageY;
            let taskbarTop = $("#taskbar").offset().top;
            let taskbarHeight = $("#taskbar").outerHeight();

            if (currentX > taskbarLeft && currentX < taskbarLeft + taskbarWidth &&
                currentY > taskbarTop + taskbarHeight) {
                let titlePosition = Math.floor((currentX - taskbarLeft) / titleWidth);
                $("td.car-title").css("background", "#fdf8e3");
                $(`td[data-row-id=${titlePosition}]`).css("background", "yellow");
            } else {
                $("td.car-title").css("background", "#fdf8e3");
            }
        });
    }

    // Get car title on click
    $(document).mousedown(function(event) {
        mouseDownX = event.pageX;
    });

    // float calendar title
    $(".fc-day-header").css("height", "26px");

    let start_pos = clr.offset().top -5;
    relativeAdjusted = 70;
    let carTitle = $('#car-title');
    carTitle.css("top", start_pos + relativeAdjusted);

    $(window).scroll(function () {
        let pos = clr.offset().top,
            scroll = $(window).scrollTop();
        if (scroll >= pos + relativeAdjusted) {
            carTitle.css("top", "0");
        } else {
            carTitle.css("top", pos - scroll + relativeAdjusted - 9);
        }
    });

    // Change Calendar URL
    $(".fc-next-button, .fc-prev-button, .fc-today-button").click(function () {
        let newDate = clr.fullCalendar('getDate').format("YYYY-MM-DD");
        changeUrl(newDate);
    });

    // Navigation
    $(".dropdown").hover(
        function () {
            $(this).find("> .dropdown-menu").show()
        }, function () {
            $(this).find(".dropdown-menu").hide()
        }
    );

    // Car picker
    $("#car-display").change(function () {
        val = $(this).val();
        $(".calendar-slots").each(function () {
            $this = $(this);
            if (val == 0) {
                $this.show();
            } else if (val == $this.attr("data-position")) {
                $this.show();
            } else {
                $this.hide();
            }
        })
    })

    /**
     * CRUD local events
     */

    // Handle Approve/ Reject event
    $("body").on("click", ".reject", function () {
        let slot_id = $(this).data("slot-id");

        $.ajax({
            url: `/slots/${slot_id}`,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
            },
            type: "PUT",
            data: {
                "slot[action]": "reject event",
                "slot[status]": "rejected"
            },
            success: function (resp) {
                showResp(resp)
            }
        });
    });

    // Handle Edit event
    $("body").on("click", ".edit", function () {
        count = 0;
        event.preventDefault();
        let slot_id = $(this).data("slot-id");

        $.ajax({
            url: `/api/slot/${slot_id}`,
            type: "GET",
            success: function (slot) {
                // Turn 09:00:00+07:00 to 09:00:00Z
                start = slot.start.replace("+07:00", "Z");
                end = slot.end.replace("+07:00", "Z");
                editEvent = new jBox('Modal', {
                    closeOnEsc: true,
                    closeButton: 'title',
                    title: "<h4>Car</h4>",
                    content: initEvent(moment(start), moment(end), slot.car.car_id, slot.invitation),
                    minWidth: 450,
                });

                editEvent.open();
                appendInvitationSuggestions(invitations);
                editTime(moment(start), moment(end));

                $("input[name='slot[title]']").val(slot.title);
                $("input[name='slot[utilizer]']").val(slot.utilizer);
                $("input[name='slot[driver]']").val(slot.driver);
                $("textarea[name='slot[description]']").val(slot.description);

                $(".form-submit").addClass("form-edit").removeClass("form-submit");
            }
        });

        // Edit Event
        $("body").on("click", ".form-edit", function () {
            count += count + 1;
            if (count == 1) {
                event.preventDefault();
                let parent = $(this).parent();
                editEvent.close();
                data = getData(parent);
                data["slot[action]"] = "updated";

                $.ajax({
                    url: `/slots/${slot_id}`,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
                    },
                    method: "PUT",
                    data: data,
                    success: function (resp) {
                        showResp(resp);
                    }
                })
                    .fail(function (resp) {
                        console.log(resp.responseJSON);
                    });
            }
        });

    });

    function getData(parent) {
        let invitation = getTags("#invitation-div", parent);

        data = {
            "slot[creator_id]": parent.find("input[name='slot[creator_id]']").val(),
            "slot[car_id]": parent.find("#car-picker").val(),
            "slot[title]": parent.find("input[name='slot[title]']").val(),
            "slot[utilizer]": parent.find("input[name='slot[utilizer]']").val(),
            "slot[driver]": parent.find("input[name='slot[driver]']").val(),
            "slot[description]": parent.find("textarea[name='slot[description]']").val(),
            "slot[start_time]": $("#new-slot #slot_start").val(),
            "slot[end_time]": $("#new-slot #slot_end").val(),
            "slot[invitation]": invitation,
            "slot[status]": "approved"
        };
        return data;
    }

    // Update event
    function updateEvent(slot, revertFunc) {
        // Update local slot
        $.ajax({
            url: `/slots/${slot.id}`,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-Token", clr.data("csrf-token"));
            },
            method: "PUT",
            data: {
                "slot[action]": "update event",
                "slot[start_time]": slot.start.utc().format(),
                "slot[end_time]": slot.end.utc().format()
            },
            success: function (resp) {
                if (resp.errors || resp.error) {
                    revertFunc();
                }
                showResp(resp)
            }
        })
    }

    /*
    ** Supporting function
     */

    // Show response of ajax call
    function showResp(resp) {
        if (resp.errors) {
            let errors = resp.errors;
            toastr.error(Object.keys(errors)[0] + ": " + errors[Object.keys(errors)[0]]);

        } else if (resp.error) {
            if (resp.error.indexOf("403") != -1) {
                resp.error = `${resp.error} - Need to ask admin to share calendar with this email, then synchronize`
            }
            toastr.error(resp.error)
        } else if (resp.success) {
            toastr.success(resp.success);
            clr.fullCalendar("refetchEvents");
        }
    }

    function isAdmin() {
        return clr.data("user-role") == "admin" || clr.data("user-role") == "super-admin";
    }

    function isOwner(userId, event) {
        return userId == event.creator_id;
    }
});
