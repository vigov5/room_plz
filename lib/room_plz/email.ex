defmodule RoomPlz.Email do
  use Phoenix.Swoosh, view: Coherence.EmailView, layout: {Coherence.LayoutView, :email}
  alias Swoosh.Email
  alias RoomPlz.Coherence.Mailer

  def send_error_email(source, target, msg) do
    %Email{}
    |> from(source)
    |> to(target)
    |> subject("An Error on Scheduling Framgia Server")
    |> text_body(msg)
    |> Mailer.deliver
  end

  def send_notification_email(target, slot, action) do
    sender = Application.get_env(:room_plz, :sender)

    %Email{}
    |> to(target)
    |> from(sender)
    |> subject("An booking has been #{action}")
    |> render_body("notification.html", %{slot: slot})
    |> Mailer.deliver
  end
end