defmodule RoomPlz.Tools do

  use Timex

  @date_format_str "{YYYY}-{0M}-{0D}"
  @datetime_format %{datetime_str: "%Y-%m-%d %H:%M", full_str: "%Y-%m-%dT%H:%M:%S%:z", time_str: "%Y-%m-%dT%H:%M:%SZ"}
  @time_shift %{utc: [hours: -7], local_time_str: [hours: 7]}

  def date_str_to_timex(date_str) do
    date_str
    |> Timex.parse!(@date_format_str)
  end

  def date_str_to_ecto(date_str, format) do
    date_str
    |> date_str_to_timex
    |> Timex.shift(Map.get(@time_shift, format))
    |> Ecto.DateTime.cast!
  end

  def date_str_to_datetime(date_str, :local_time_str = type) do
    {:ok, datetime, _} = DateTime.from_iso8601(date_str)

    datetime =
      datetime
      |> Timex.shift(Map.get(@time_shift, type))
      |> DateTime.to_string()

    match_array = Regex.run(~r/\s(.*):00Z/, datetime)
    Enum.at(match_array, 1)
  end

  def date_str_to_datetime(date_str, :utc = type) do
    date_str
      |> Timex.parse!("{ISO:Extended:Z}")
      |> Timex.shift(Map.get(@time_shift, type))
  end

  def date_str_to_datetime(date_str, :date) do
    date_str
      |> Timex.parse!("{ISO:Extended:Z}")
      |> DateTime.to_date()
  end

  def date_str_to_datetime(date_str, :time_str) do
    date_str
      |> Ecto.DateTime.cast!()
      |> ecto_to_local_datetime(:time_str)
  end

  def local_date_str_to_datetime(date_str) do
    ~r/(.*)\+07\:00/
    |> Regex.replace(date_str, "\\1Z")
    |> date_str_to_datetime(:utc)
  end

  def ecto_to_local_datetime(datetime) do
    datetime
    |> Ecto.DateTime.to_erl
    |> NaiveDateTime.from_erl!
    |> naive_to_local_datetime
  end

  def ecto_to_local_datetime(datetime, :date) do
    datetime
    |> ecto_to_local_datetime
    |> DateTime.to_date()
  end

  def ecto_to_local_datetime(datetime, :date_str) do
    datetime
    |> ecto_to_local_datetime(:date)
    |> Date.to_string()
  end

  def ecto_to_local_datetime(datetime, :time_str) do
    datetime
    |> ecto_to_local_datetime
    |> DateTime.to_time()
    |> Time.to_string()
  end

  def ecto_to_local_datetime(date_time, :shorten_time_str) do
    date_time
    |> ecto_to_local_datetime(:time_str)
    |> String.slice(0..-4)
  end

  def ecto_to_local_datetime(datetime, format) do
    datetime
    |> ecto_to_local_datetime
    |> Timex.format!(Map.get(@datetime_format, format), :strftime)
  end

  def naive_to_local_datetime(datetime) do
    timezone = Timezone.get("Asia/Ho_Chi_Minh", Timex.now)

    datetime
    |> DateTime.from_naive!("Etc/UTC")
    |> Timezone.convert(timezone)
  end

  def naive_to_local_datetime(datetime, format) do
    datetime
    |> naive_to_local_datetime
    |> Timex.format!(Map.get(@datetime_format, format), :strftime)
  end

  def get_current_date() do
    timezone = Timezone.get("Asia/Ho_Chi_Minh", Timex.now)

    DateTime.utc_now()
    |> Timezone.convert(timezone)
    |> DateTime.to_date
  end
end
