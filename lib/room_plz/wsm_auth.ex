defmodule WSM do
  @moduledoc """
  An OAuth2 strategy for WSM Framgia.
  """
  use OAuth2.Strategy

  alias OAuth2.Strategy.AuthCode

  defp config do
    [strategy: WSM,
      site: "https://wsm.framgia.vn",
      authorize_url: "/authorize",
      token_url: "/auth/access_token"]
  end

  # Public API

  def client do
    :room_plz
    |> Application.get_env(WSM)
    |> Keyword.merge(config())
    |> OAuth2.Client.new()
  end

  def authorize_url!(params \\ []) do
    OAuth2.Client.authorize_url!(client(), params)
  end

  def get_token(params \\ []) do
    params =
      :room_plz
      |> Application.get_env(WSM)
      |> Keyword.merge(params)
      |> Keyword.put(:response_type, nil)

    OAuth2.Client.get_token(client(), params)
  end

  # Strategy Callbacks

  def authorize_url(client, params) do
    AuthCode.authorize_url(client, params)
  end

  def get_token(client, params, headers) do
    client
    |> put_header("Accept", "application/json")
    |> AuthCode.get_token(params, headers)
  end
end