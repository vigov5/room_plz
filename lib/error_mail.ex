defmodule Logger.Backends.ErrorMail do
  use GenEvent
  require Logger
  alias RoomPlz.Email

  def init(_) do
   if user = Process.whereis(:user) do
      Process.group_leader(self(), user)
      Logger.info("Started")
      {:ok, configure([])}
    else
      {:ok, configure([])}
    end
  end

  def handle_call({:configure, options}, _state) do
    {:ok, :ok, configure(options)}
  end

  def handle_event({:error, _gl, {_, msg, ts, md}}, state) do
    log_event(msg, ts, md, state)
    {:ok, state}
  end

  # catchall
  def handle_event(_event, state) do
    {:ok, state}
  end

  defp log_event(msg, ts, md, {source, targets, format, metadata}) do
    msg =
      format
      |> Logger.Formatter.format(:error, msg, ts, Dict.take(md, metadata))
      |> IO.iodata_to_binary

    Enum.each(targets, fn target -> Email.send_error_email(source, target, msg) end)
  end

  defp configure(options) do
    error_mail = Keyword.merge(Application.get_env(:logger, :error_mail, []), options)
    Application.put_env(:logger, :error_mail, error_mail)

    to  = Keyword.get(error_mail, :to)
    from     = Keyword.get(error_mail, :from)
    metadata = Keyword.get(error_mail, :metadata, [])

    format = Logger.Formatter.compile(Keyword.get(error_mail, :format))
    {from, to, format, metadata}
  end
end
