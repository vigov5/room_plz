defimpl Canada.Can, for: RoomPlz.User do
  alias RoomPlz.{Branch, Room, User, Event, Car, Team}

  def can?(user, action, Room) when action in [:index, :new, :create] do
    User.is_admin?(user)
  end

  def can?(user, action, %Room{}) when action in [:show, :edit, :update, :delete] do
    User.is_admin?(user)
  end

  def can?(user, action, Team) when action in [:index, :new, :create] do
    User.is_admin?(user)
  end

  def can?(user, action, %Team{}) when action in [:show, :edit, :update, :delete] do
    User.is_admin?(user)
  end

  def can?(user, action, Branch) when action in [:index, :new, :create] do
    User.is_admin?(user)
  end

  def can?(user, action, %Branch{}) when action in [:show, :edit, :update, :delete] do
    User.is_admin?(user)
  end

  # User
  def can?(user, action, User) when action in [:index, :new, :create] do
    User.is_admin?(user)
  end

  def can?(%User{id: current_user_id} = user, action, %User{id: user_id, role: role})
    when action in [:show, :edit, :update, :delete] and role != "super-admin" do

    User.is_admin?(user) || User.is_owner(current_user_id, user_id)
  end

  def can?(%User{id: current_user_id} = user, action, %User{id: user_id})
      when action in [:show, :edit, :update, :delete] do

    User.is_super_admin?(user) || User.is_owner(current_user_id, user_id)
  end

  # Event
  def can?(user, action, Event) when action in [:index, :new, :create] do
    User.is_admin?(user)
  end

  def can?(_user, action, %Event{}) when action in [:show] do
    true
  end

  def can?(%User{id: current_user_id} = user, action, %Event{creator_id: user_id, collaborators: collaborators})
    when action in [:edit, :update, :delete] do

    User.is_admin?(user) || User.is_owner(current_user_id, user_id) || User.is_collaborator(current_user_id, collaborators)
  end

  #  Car
  def can?(user, action, Car) when action in [:index, :new, :create] do
    User.is_admin?(user)
  end

  def can?(user, action, %Car{}) when action in [:show, :edit, :update, :delete] do
    User.is_admin?(user)
  end

  def can?(_, _, nil), do: true
  def can?(%User{}, _, _), do: false
end
