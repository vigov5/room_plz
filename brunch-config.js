exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
        joinTo: "js/app.js"

      // To use a separate vendor.js bundle, specify two files path
      // http://brunch.io/docs/config#-files-
      // joinTo: {
      //  "js/app.js": /^(web\/static\/js)/,
      //  "js/vendor.js": /^(web\/static\/vendor)|(deps)/
      // }
      //
      // To change the order of concatenation of files, explicitly mention here
      // order: {
      //   before: [
      //     "web/static/vendor/js/jquery-2.1.1.js",
      //     "web/static/vendor/js/bootstrap.min.js"
      //   ]
      // }
    },
    stylesheets: {
      joinTo: "css/app.css",
      order: {
        after: ["web/static/css/app.css"] // concat app.css last
      }
    },
    templates: {
      joinTo: "js/app.js"
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/web/static/assets". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(web\/static\/assets)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: [
      "web/static",
      "test/static"
    ],

    // Where to compile files to
    public: "priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [/web\/static\/vendor/, /web\/static\/calendar/]
    }
  },

  modules: {
    autoRequire: {
      "js/app.js": ["web/static/js/app"]
    }
  },

  npm: {
    enabled: true,
    globals: {
      $: 'jquery',
      jQuery: 'jquery',
      bootstrap: 'bootstrap',
      flatpickr: 'flatpickr',
      moment: 'moment',
      'moment-timezone': 'moment-timezone',
      'jbox': 'jbox',
      fullcalendar: 'fullcalendar',
      toastr: 'toastr',
      typeahead: 'typeahead',
      'boostrap-select': 'bootstrap-select',
      'boostrap-tagsinput': 'bootstrap-tagsinput',
      'datatables.net': 'datatables.net',
      'datatable.net-bs': 'datatables.net-bs'
    },
    styles: {
      flatpickr: ['dist/flatpickr.min.css'],
      fullcalendar: ['dist/fullcalendar.min.css'],
      toastr: ['build/toastr.min.css'],
      'bootstrap-select': ['dist/css/bootstrap-select.css'],
      'bootstrap-tagsinput': ['dist/bootstrap-tagsinput-typeahead.css', 'src/bootstrap-tagsinput.css'],
      'jbox': ['Source/jBox.css']
    }
  }
};
