# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs

# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RoomPlz.Repo.insert!(%RoomPlz.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias RoomPlz.{Repo, User}

user = Repo.get_by(User, email: "nguyen.dinh.tung@framgia.com")

if (!user) do
  User.changeset(%User{}, %{name: "dinhtungtp", role: "super-admin", email: "nguyen.dinh.tung@framgia.com", password: "tung123", password_confirmation: "tung123"})
  |> Repo.insert!
  |> Coherence.ControllerHelpers.confirm!
end