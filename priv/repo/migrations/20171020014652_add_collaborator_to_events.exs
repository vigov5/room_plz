defmodule RoomPlz.Repo.Migrations.AddCollaboratorToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :collaborators, :string
    end
  end
end
