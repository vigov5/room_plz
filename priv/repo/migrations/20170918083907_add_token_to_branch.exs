defmodule RoomPlz.Repo.Migrations.AddTokenToBranch do
  use Ecto.Migration

  def change do
    alter table(:branches) do
      add :auth_email, :string
      add :refresh_token, :string
    end
  end
end
