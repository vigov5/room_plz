defmodule RoomPlz.Repo.Migrations.AddCalendarIdToBranches do
  use Ecto.Migration

  def change do
    alter table(:branches) do
      add :calendar_id, :string
    end
  end
end
