defmodule RoomPlz.Repo.Migrations.CreateSlotLog do
  use Ecto.Migration

  def change do
    create table(:slot_logs) do
      add :action, :string
      add :detail, :string
      add :utilizer, :string
      add :driver, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :slot_id, references(:slots, on_delete: :nothing)

      timestamps()
    end
    create index(:slot_logs, [:user_id])
    create index(:slot_logs, [:slot_id])

  end
end
