defmodule RoomPlz.Repo.Migrations.CreateRoom do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :name, :string
      add :capacity, :integer
      add :color, :string
      add :branch_id, references(:branches, on_delete: :nothing)

      timestamps()
    end
    create index(:rooms, [:branch_id])

  end
end
