defmodule RoomPlz.Repo.Migrations.AddDateArrayToEvent do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :date_array, {:array, :string}
      add :is_current, :boolean
    end
  end
end
