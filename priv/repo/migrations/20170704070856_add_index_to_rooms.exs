defmodule RoomPlz.Repo.Migrations.AddIndexToRooms do
  use Ecto.Migration

  def change do
    create unique_index(:rooms, [:name, :branch_id], name: :name_in_branch_index)
  end
end
