defmodule RoomPlz.Repo.Migrations.CreateSlot do
  use Ecto.Migration

  def change do
    create table(:slots) do
      add :title, :string
      add :start, :naive_datetime
      add :end, :naive_datetime
      add :status, :string
      add :description, :string
      add :utilizer, :string
      add :driver, :string
      add :invitation, :string
      add :creator_id, references(:users, on_delete: :nothing)
      add :car_id, references(:cars, on_delete: :nothing)

      timestamps()
    end
    create index(:slots, [:creator_id])
    create index(:slots, [:car_id])

  end
end
