defmodule RoomPlz.Repo.Migrations.AddInvitationListToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :invitation_list, :string
    end
  end
end
