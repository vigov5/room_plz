defmodule RoomPlz.Repo.Migrations.AddInvitationToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :invitation, :string
    end
  end
end
