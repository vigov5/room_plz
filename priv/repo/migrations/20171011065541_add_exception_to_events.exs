defmodule RoomPlz.Repo.Migrations.AddExceptionToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :date_exception, {:array, :string}
    end
  end
end
