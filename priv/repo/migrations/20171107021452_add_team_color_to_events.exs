defmodule RoomPlz.Repo.Migrations.AddTeamColorToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :color, :string
      add :team_id, references(:teams, on_delete: :nothing)
    end

    create index(:events, [:team_id])
  end
end
