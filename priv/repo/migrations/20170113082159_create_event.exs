defmodule RoomPlz.Repo.Migrations.CreateEvent do
  use Ecto.Migration

  def change do
    StatusEnum.create_type
    create table(:events) do
      add :title, :string
      add :start, :naive_datetime
      add :end, :naive_datetime
      add :status, :status
      add :room_id, references(:rooms, on_delete: :nothing)
      add :creator_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:events, [:room_id])
    create index(:events, [:creator_id])

  end
end
