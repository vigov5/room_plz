defmodule RoomPlz.Repo.Migrations.AddDescriptionOverlapToRooms do
  use Ecto.Migration

  def change do
    alter table(:rooms) do
      add :description, :string
      add :overlap_acceptance, :boolean
    end
  end
end
