defmodule RoomPlz.Repo.Migrations.CreateCar do
  use Ecto.Migration

  def change do
    create table(:cars) do
      add :name, :string
      add :capacity, :integer
      add :description, :string
      add :color, :string

      timestamps()
    end

    create unique_index(:cars, [:name], name: :car_name_index)
  end
end
