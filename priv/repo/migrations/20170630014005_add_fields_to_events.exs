defmodule RoomPlz.Repo.Migrations.AddFieldsToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :description, :string
      add :repeat, :string
      add :frequency, :string
      add :interval, :integer
      add :weekday, :string
      add :count, :integer
      add :until, :date
    end
  end
end
