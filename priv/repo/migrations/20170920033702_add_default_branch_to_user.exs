defmodule RoomPlz.Repo.Migrations.AddDefaultBranchToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :branch_id, references(:branches, on_delete: :nothing)
    end
  end
end
