defmodule RoomPlz.Repo.Migrations.AddDefaultTeamToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :team_id, :integer
    end
  end
end
