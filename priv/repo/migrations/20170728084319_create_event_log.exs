defmodule RoomPlz.Repo.Migrations.CreateEventLog do
  use Ecto.Migration

  def change do
    create table(:event_logs) do
      add :action, :string
      add :detail, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :branch_id, references(:branches, on_delete: :nothing)
      add :event_id, references(:events, on_delete: :nothing)

      timestamps()
    end

    create index(:event_logs, [:user_id])
    create index(:event_logs, [:branch_id])
    create index(:event_logs, [:event_id])

  end
end
