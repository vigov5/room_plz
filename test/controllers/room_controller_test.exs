defmodule RoomPlz.RoomControllerTest do
  use RoomPlz.ConnCase
  import RoomPlz.TestHelpers
  alias RoomPlz.Room

  @moduletag :room_test
  @valid_attrs %{capacity: 42, color: "blue", name: "Kularlumpur"}
  @invalid_attrs %{color: 43}

  setup %{conn: conn} = config do
    branch = insert_branch()
    if email = config[:login_as] do
      user = insert_user(%{"email" => email})
      conn = assign conn, :current_user, user
      {:ok, conn: conn, user: user, branch: branch}
    else
      user = insert_user()
      {:ok, branch: branch, user: user}
    end
  end

  @tag login_as: "index@framgia.com"
  test "lists all entries on index", %{conn: conn, branch: branch} do
    conn = get conn, branch_room_path(conn, :index, branch)
    assert html_response(conn, 200) =~ "Listing rooms"
  end

  @tag login_as: "new@framgia.com"
  test "renders form for new resources", %{conn: conn, branch: branch} do
    conn = get conn, branch_room_path(conn, :new, branch)
    assert html_response(conn, 200) =~ "New room"
  end

  @tag login_as: "create@framgia.com"
  test "creates resource and redirects when data is valid", %{conn: conn, branch: branch} do
    attrs = Map.merge(@valid_attrs, %{branch_id: branch.id})
    conn = post conn, branch_room_path(conn, :create, branch), room: attrs
    assert redirected_to(conn) == branch_room_path(conn, :index, branch)
    assert Repo.get_by(Room, @valid_attrs)
  end

  @tag login_as: "create@framgia.com"
  test "does not create resource and renders errors when data is invalid", %{conn: conn, branch: branch} do
    attrs = Map.merge(@invalid_attrs, %{branch_id: branch.id})
    conn = post conn, branch_room_path(conn, :create, branch), room: attrs
    assert html_response(conn, 200) =~ "New room"
  end

  @tag login_as: "show@framgia.com"
  test "shows chosen resource", %{conn: conn, branch: branch} do
    room = insert_room(%{branch_id: branch.id})
    conn = get conn, branch_room_path(conn, :show, branch, room)
    assert html_response(conn, 200) =~ "Show room"
  end

  @tag login_as: "show@framgia.com"
  test "renders page not found when id is nonexistent", %{conn: conn, branch: branch} do
    conn = get conn, branch_room_path(conn, :show, branch, -1)
    assert html_response(conn, 404) =~ "Page not found"
  end

  @tag login_as: "edit@framgia.com"
  test "renders form for editing chosen resource", %{conn: conn, branch: branch} do
    room = insert_room(%{branch_id: branch.id})
    conn = get conn, branch_room_path(conn, :edit, branch, room)
    assert html_response(conn, 200) =~ "Edit room"
  end

  @tag login_as: "delete@framgia.com"
  test "deletes chosen resource", %{conn: conn, branch: branch} do
    room = insert_room(%{branch_id: branch.id})
    conn = delete conn, branch_room_path(conn, :delete, branch, room)
    assert redirected_to(conn) == branch_room_path(conn, :index, branch)
    refute Repo.get(Room, room.id)
  end
end
