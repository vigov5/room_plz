defmodule RoomPlz.PageControllerTest do
  use RoomPlz.ConnCase
  import RoomPlz.TestHelpers

  @moduletag :page_test

  test "GET /", %{conn: conn} do
    insert_branch();
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Framgia"
  end
end
