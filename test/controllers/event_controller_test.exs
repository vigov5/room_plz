defmodule RoomPlz.EventControllerTest do
  use RoomPlz.ConnCase
  import RoomPlz.TestHelpers
  alias RoomPlz.Event

  @moduletag :event_test

  setup %{conn: conn} = config do
    branch = insert_branch()
    if email = config[:login_as] do
      user = insert_user(%{"email" => email})
      team = insert_team()
      conn = assign conn, :current_user, user
      {:ok, conn: conn, user: user, branch: branch, team: team}
    else
      user = insert_user()
      {:ok, branch: branch, user: user}
    end
  end

  test "authentication on routes", %{conn: conn, branch: branch} do
    Enum.each([
      get(conn, branch_event_path(conn, :new, branch)),
      post(conn, branch_event_path(conn, :create, branch, %{})),
      get(conn, branch_event_path(conn, :edit, branch, 123)),
      put(conn, branch_event_path(conn, :update, branch, 123, %{})),
      delete(conn, branch_event_path(conn, :delete, branch, 123)),
    ], fn conn ->
      assert String.contains?(conn.resp_body, "Sign In")
    end)
  end

  @tag login_as: "index@framgia.com"
  test "lists all entries on index", %{conn: conn, user: user, branch: branch, team: team} do
    room = insert_room(%{branch_id: branch.id})
    insert_event(%{"room_id" => room.id, "creator_id" => user.id, "team_id" => team.id})
    conn = get conn, branch_event_path(conn, :index, branch)
    assert html_response(conn, 200) =~ "Listing events"
  end

  @tag login_as: "new@framgia.com"
  test "renders form for new resources", %{conn: conn, user: _user, branch: branch} do
    conn = get conn, branch_event_path(conn, :new, branch)
    assert html_response(conn, 200) =~ "New event"
  end

  @tag login_as: "show@framgia.com"
  test "shows chosen resource", %{conn: conn, branch: branch, user: user, team: team} do
    room = insert_room(%{branch_id: branch.id})
    event = insert_event(%{"room_id" => room.id, "creator_id" => user.id, "team_id" => team.id})
    conn = get conn, branch_event_path(conn, :show, branch, event)
    assert html_response(conn, 200) =~ "Show event"
  end

  @tag login_as: "show@framgia.com"
  test "renders page not found when id is nonexistent", %{conn: conn, branch: branch} do
    conn = get conn, branch_event_path(conn, :show, branch, -1)
    assert html_response(conn, 404) =~ "Page not found"
  end

  @tag login_as: "edit@framgia.com"
  test "renders form for editing chosen resource", %{conn: conn, user: user, branch: branch} do
    room = insert_room(%{branch_id: branch.id})
    event = insert_event(%{"room_id" => room.id, "creator_id" => user.id})
    conn = get conn, branch_event_path(conn, :edit, branch, event)
    assert html_response(conn, 200) =~ "Edit event"
  end

  @tag login_as: "delete@framgia.com"
  test "deletes chosen resource", %{conn: conn, user: user, branch: branch} do
    room = insert_room(%{branch_id: branch.id})
    event = insert_event(%{"room_id" => room.id, "creator_id" => user.id})
    conn = delete conn, branch_event_path(conn, :delete, branch, event)
    assert redirected_to(conn) == branch_event_path(conn, :index, branch)
    refute Repo.get(Event, event.id)
  end
end
