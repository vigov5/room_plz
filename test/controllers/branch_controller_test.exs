defmodule RoomPlz.BranchControllerTest do
  use RoomPlz.ConnCase
  import RoomPlz.TestHelpers
  alias RoomPlz.Branch

  @moduletag :branch_test
  @valid_attrs %{name: "HCMFC", calendar_id: "secret@gmail.com"}
  @invalid_attrs %{name: ""}

  setup %{conn: conn} = config do
    branch = insert_branch()
    if email = config[:login_as] do
      user = insert_user(%{"email" => email})
      conn = assign conn, :current_user, user
      {:ok, conn: conn, user: user, branch: branch}
    else
      user = insert_user()
      {:ok, branch: branch, user: user}
    end
  end

  @tag login_as: "index@framgia.com"
  test "lists all entries on index", %{conn: conn} do
    conn = get conn, branch_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing branches"
  end

  @tag login_as: "new@framgia.com"
  test "renders form for new resources", %{conn: conn} do
    conn = get conn, branch_path(conn, :new)
    assert html_response(conn, 200) =~ "New branch"
  end

  @tag login_as: "create@framgia.com"
  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, branch_path(conn, :create), branch: @valid_attrs
    assert redirected_to(conn) == branch_path(conn, :index)
    assert Repo.get_by(Branch, @valid_attrs)
  end

  @tag login_as: "create@framgia.com"
  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, branch_path(conn, :create), branch: @invalid_attrs
    assert html_response(conn, 200) =~ "New branch"
  end

  @tag login_as: "show@framgia.com"
  test "shows chosen resource", %{conn: conn, branch: branch} do
    conn = get conn, branch_path(conn, :show, branch)
    assert html_response(conn, 200) =~ "Show branch"
  end

  @tag login_as: "show@framgia.com"
  test "renders page not found when id is nonexistent", %{conn: conn} do
    conn = get conn, branch_path(conn, :show, -1)
    assert html_response(conn, 404) =~ "Page not found"
  end

  @tag login_as: "edit@framgia.com"
  test "renders form for editing chosen resource", %{conn: conn, branch: branch} do
    conn = get conn, branch_path(conn, :edit, branch)
    assert html_response(conn, 200) =~ "Edit branch"
  end

  @tag login_as: "update@framgia.com"
  test "updates chosen resource and redirects when data is valid", %{conn: conn, branch: branch} do
    conn = put conn, branch_path(conn, :update, branch), branch: @valid_attrs
    assert redirected_to(conn) == branch_path(conn, :show, branch)
    assert Repo.get_by(Branch, @valid_attrs)
  end

  @tag login_as: "update@framgia.com"
  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn, branch: branch} do
    conn = put conn, branch_path(conn, :update, branch), branch: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit branch"
  end

  @tag login_as: "delete@framgia.com"
  test "deletes chosen resource", %{conn: conn, branch: branch} do
    conn = delete conn, branch_path(conn, :delete, branch)
    assert redirected_to(conn) == branch_path(conn, :index)
    refute Repo.get(Branch, branch.id)
  end

end
