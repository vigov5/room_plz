defmodule RoomPlz.TestHelpers do
  import RoomPlz.ConnCase
  alias RoomPlz.{Repo, User, Branch, Room, Event, Team}

  def insert_user(attrs \\ %{}) do
    changes = Map.merge(%{
      "email" => "user1@framgia.com",
      "name" => "key user",
      "password" => "supersecret",
      "password_confirm" => "supersecret",
      "role" => "super-admin"
    }, attrs)

    %User{}
    |> User.changeset(changes)
    |> Repo.insert!()
  end

  def insert_branch(attrs \\ %{}) do
    changes = Map.merge(%{
      name: "Hanoi",
      calendar_id: "calendar_id@framgia.com",
      refresh_token: Application.get_env(:room_plz, :refresh_token)
    }, attrs)

    %Branch{}
    |> Branch.changeset(changes)
    |> Repo.insert!()
  end

  def insert_room(attrs \\ %{}) do
    changes = Map.merge(%{
      name: "Booth1",
      capacity: 5,
      color: "red"
    }, attrs)

    %Room{}
    |> Room.changeset(changes)
    |> Repo.insert!()
  end

  def insert_event(attrs \\ %{}) do
    changes = Map.merge(%{
      "end" => "2017-07-30T14:33:32Z",
      "start" => "2017-07-30T09:33:32Z",
      "status" => "approved",
      "title" => "Event2",
      "repeat" => "Every 2 day(s)",
      "frequency" => "Weekly",
      "interval" =>4,
      "until" => "2017-06-05"
    }, attrs)

    %Event{}
    |> Event.changeset(changes)
    |> Repo.insert!()
  end

  def insert_team(attrs \\ %{}) do
    changes = Map.merge(%{
      name: "Human Resource",
    }, attrs)

    %Team{}
    |> Team.changeset(changes)
    |> Repo.insert!()
  end
end