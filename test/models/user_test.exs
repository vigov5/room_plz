defmodule RoomPlz.UserTest do
  use RoomPlz.ModelCase
  import RoomPlz.TestHelpers

  @moduletag :module_test

  test "creates a user" do
    user = insert_user()
    assert user.email == "user1@framgia.com"
  end
end
