defmodule RoomPlz.RoomTest do
  use RoomPlz.ModelCase
  import RoomPlz.TestHelpers
  alias RoomPlz.Room

  @moduletag :module_test
  @valid_attrs %{capacity: 42, color: "some content", name: "some content", branch_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Room.changeset(%Room{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Room.changeset(%Room{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "create new room" do
    branch = insert_branch()
    room = insert_room(%{branch_id: branch.id})
    assert room.name == "Booth1"
  end
end
