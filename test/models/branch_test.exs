defmodule RoomPlz.BranchTest do
  use RoomPlz.ModelCase
  import RoomPlz.TestHelpers
  alias RoomPlz.Branch

  @moduletag :module_test
  @valid_attrs %{name: "some content", calendar_id: "abc@gmail.com"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Branch.changeset(%Branch{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Branch.changeset(%Branch{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "create new branch" do
    branch = insert_branch()
    assert branch.name == "Hanoi"
  end
end
