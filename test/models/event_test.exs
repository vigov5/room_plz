defmodule RoomPlz.EventTest do
  use RoomPlz.ModelCase
  import RoomPlz.TestHelpers
  alias RoomPlz.Event

  @moduletag :module_test
  @valid_attrs %{"end" => "2019-06-30T14:33:32Z", "start" => "2017-06-30T09:33:32Z",
                "status" => "pending", "title" => "some content", "repeat" => "Every 1 day(s)",
                "frequency" => "Weekly", "interval" => 2, "count" => 5, "room_id" => 1, "creator_id" => 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    branch = insert_branch();
    room = insert_room(%{branch_id: branch.id})
    attrs = Map.put(@valid_attrs, "room_id", room.id)

    changeset = Event.changeset(%Event{}, attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Event.changeset(%Event{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "create new event" do
    user = insert_user()
    branch = insert_branch()
    room = insert_room(%{branch_id: branch.id})
    event = insert_event(%{"creator_id" => user.id, "room_id" => room.id})

    assert event.title == "[Booth1] Event2"
    assert event.repeat == "Every 2 day(s)"
  end

  test "test scrub params" do
    user = insert_user()
    branch = insert_branch()
    room = insert_room(%{branch_id: branch.id})

    event =
      %{"creator_id" => user.id, "room_id" => room.id}
      |> insert_event()
      |> Event.changeset(%{"repeat" => nil})
      |> Repo.update!()

    assert is_nil(event.repeat)
  end

  test "check cases of repeat field" do
    # when repeat is empty, other field is set to nil
    user = insert_user()
    branch = insert_branch()
    room = insert_room(%{branch_id: branch.id})

    attrs = Map.merge(@valid_attrs, %{"repeat" => nil, "creator_id" => user.id, "room_id" => room.id})
    event = insert_event(attrs)
    assert is_nil(event.frequency)
    assert is_nil(event.interval)
    assert is_nil(event.count)

    # Count and until are mutual exclusive when repeat are available
    event =
      event
      |> Event.changeset(%{"repeat" => "Every 1 day(s)", "count" => 1, "frequency" => "Daily", "interval" => 4})
      |> Repo.update!()

    refute is_nil(event.repeat)
    assert event.count == 1

    event =
      event
      |> Event.changeset(%{"repeat" => "Every 2 day(s)", "until" => "2017-08-06"})
      |> Repo.update!()

    assert is_nil(event.count)
  end
end
